<?php

use Phinx\Migration\AbstractMigration;

class CreateGroupPermissionTable extends AbstractMigration
{

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('auth_group_permission', ['id' => false]);
        $table
            ->addColumn('group_id', 'uuid', ['null' => false])
            ->addColumn('permission_id', 'uuid', ['null' => false])
            ->addIndex(['group_id', 'permission_id'], ['unique' => true, 'name' => 'idx_gp_permission_group'])

            ->addForeignKey('group_id', 'auth_group', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addForeignKey('permission_id', 'auth_permission', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('auth_group_permission');
    }
}
