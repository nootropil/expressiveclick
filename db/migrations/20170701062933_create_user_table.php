<?php

use Phinx\Migration\AbstractMigration;

//vendor/bin/phinx create MyNewMigration
class CreateUserTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('user', ['id' => false]);
        $table
            ->addColumn('id', 'uuid', ['null' => false])
            ->addColumn('username', 'string', ['null' => false, 'limit' => 64 ])
            ->addColumn('password_hash', 'string', ['null' => false, 'limit' => 128])
            ->addColumn('email', 'string', ['limit' => 64])
            ->addColumn('role', 'string', ['limit' => 64])
            ->addColumn('status', 'string', ['limit' => 64])

            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'null' => false])
            ->addColumn('updated', 'timestamp', ['default' => null, 'null' => true])

            ->addIndex(['id'], ['unique' => true, 'name' => 'idx_user_id'])
            ->addIndex(['email'], ['unique' => true, 'name' => 'idx_user_email'])
            ->addIndex(['username'], ['unique' => true, 'name' => 'idx_user_username'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
