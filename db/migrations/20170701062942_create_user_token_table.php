<?php

use Phinx\Migration\AbstractMigration;

class CreateUserTokenTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('user_token', ['id' => false]);
        $table
            ->addColumn('id', 'uuid', ['null' => false])
            ->addColumn('user_id', 'uuid', ['null' => false])
            ->addColumn('token', 'string', ['null' => false, 'limit' => 255])
            ->addColumn('type', 'string', ['null' => false, 'limit' => 64])
            ->addColumn('created', 'timestamp', ['null' => false])

            ->addIndex(['id'], ['unique' => true, 'name' => 'idx_user_token_id'])
            ->addIndex(['token', 'type'], ['name' => 'idx_user_token_token_type'])
            ->addForeignKey('user_id', 'user', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->save();
    }



    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('user_token');
    }
}
