<?php

use Phinx\Migration\AbstractMigration;

class CreateUserGroupTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('auth_user_group', ['id' => false]);
        $table
            ->addColumn('group_id', 'uuid', ['null' => false])
            ->addColumn('user_id', 'uuid', ['null' => false])
            ->addIndex(['group_id', 'user_id'], ['unique' => true, 'name' => 'idx_ug_user_group'])

            ->addForeignKey('group_id', 'auth_group', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addForeignKey('user_id', 'user', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('auth_user_group');
    }
}
