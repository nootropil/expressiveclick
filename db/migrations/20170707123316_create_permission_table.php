<?php

use Phinx\Migration\AbstractMigration;

class CreatePermissionTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('auth_permission', ['id' => false]);
        $table
            ->addColumn('id', 'uuid', ['null' => false])
            ->addColumn('name', 'string', ['null' => false, 'limit' => 64 ])
            ->addColumn('description', 'string', ['null' => false, 'limit' => 255 ])
            ->addColumn('permission_category_id', 'uuid', ['null' => true])

            ->addIndex(['id'], ['unique' => true, 'name' => 'idx_permission_id'])
            ->addIndex(['name'], ['unique' => true, 'name' => 'idx_permission_name'])
            ->addIndex(['description'], ['unique' => true, 'name' => 'idx_permission_description'])
            ->addForeignKey('permission_category_id', 'auth_permission_category', 'id', ['delete'=> 'SET NULL', 'update'=> 'CASCADE'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('auth_permission');
    }
}
