<?php

use Phinx\Migration\AbstractMigration;

class CreateGroupTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('auth_group', ['id' => false]);
        $table
            ->addColumn('id', 'uuid', ['null' => false])
            ->addColumn('name', 'string', ['null' => false, 'limit' => 64 ])
            ->addColumn('description', 'string', ['null' => false, 'limit' => 255 ])

            ->addIndex(['id'], ['unique' => true, 'name' => 'idx_group_id'])
            ->addIndex(['name'], ['unique' => true, 'name' => 'idx_group_name'])
            ->addIndex(['description'], ['unique' => true, 'name' => 'idx_group_description'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('auth_group');
    }
}
