<?php
use Phinx\Migration\AbstractMigration;

class CreateLanguageTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('localization_language', ['id' => false]);
        $table
            ->addColumn('id', 'uuid', ['null' => false])
            ->addColumn('url', 'string', ['null' => false, 'limit' => 2])
            ->addColumn('locale', 'string', ['null' => false, 'limit' => 8])
            ->addColumn('name', 'string', ['null' => false, 'limit' => 255])
            ->addColumn('main', 'char', ['null' => false, 'limit' => 1])
            ->addColumn('active', 'char', ['null' => false, 'limit' => 1])

            ->addIndex(['id'], ['unique' => true, 'name' => 'idx_llanguage_id'])
            ->addIndex(['name'], ['unique' => true, 'name' => 'idx_llanguage_name'])
            ->addIndex(['url'], ['unique' => true, 'name' => 'idx_llanguage_url'])
            ->addIndex(['locale'], ['unique' => true, 'name' => 'idx_llanguage_locale'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('localization_language');
    }
}
