<?php
declare(strict_types = 1);

namespace App\Core\Infrastructure\Exception;

final class ValidationException extends \Exception
{
}