<?php
declare(strict_types = 1);

namespace App\Core\Infrastructure\Repository\User;

use App\Core\Domain\Model\User\User;
use App\Core\Infrastructure\Repository\Hydrator;
use function Zelenin\Hydrator\createObjectWithoutConstructor;
use Zelenin\Hydrator\NamingStrategy\UnderscoreToLowerCamelCaseNamingStrategy;
use Zelenin\Hydrator\Strategy\ReflectionStrategy;
use Zelenin\Hydrator\StrategyHydrator;
use DateTime;

final class UserHydrator implements Hydrator
{
    /**
     * @param array $columns
     * @return User
     */
    public function hydrate(array $columns): User
    {
        $columns['created'] = new DateTime($columns['created']);
        $columns['updated'] =  $columns['updated'] !== null ? new DateTime($columns['updated']): null;
        $hydrator = new StrategyHydrator(new ReflectionStrategy(), new UnderscoreToLowerCamelCaseNamingStrategy());
        /* @var $object User */
        $object = createObjectWithoutConstructor(User::class);
        $object = $hydrator->hydrate($object, $columns);
        return $object;
    }
}