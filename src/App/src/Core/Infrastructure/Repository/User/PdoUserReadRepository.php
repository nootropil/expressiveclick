<?php
declare(strict_types=1);

namespace App\Core\Infrastructure\Repository\User;

use App\Core\Domain\Model\User\User;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Infrastructure\Repository\Hydrator;
use PDO;

final class PdoUserReadRepository implements UserReadRepository
{
    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var Hydrator
     */
    private $hydrator;

    /**
     * PdoUserReadRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->hydrator = new UserHydrator();
        $this->pdo = $pdo;
    }

    /**
     * @param $id
     * @return User
     */
    public function fetch(string $id): ?User
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserRepository::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserRepository::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function fetchByEmail(string $email): ?User
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserRepository::TABLE_NAME . ' WHERE email = :email');
        $stmt->execute([':email' => $email]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }


    /**
     * @param string $email
     * @return bool
     */
    public function existsByEmail(string $email): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserRepository::TABLE_NAME . ' WHERE email = :email');
        $stmt->execute([':email' => $email]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param $username
     * @return User|null
     */
    public function fetchByUsername(string $username): ?User
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserRepository::TABLE_NAME . ' WHERE username = :username');
        $stmt->execute([':username' => $username]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $username
     * @return bool
     */
    public function existsByUsername(string $username): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserRepository::TABLE_NAME . ' WHERE username = :username');
        $stmt->execute([':username' => $username]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @return User[]
     */
    public function fetchAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserRepository::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $users = [];
        foreach ($rows as $row) {
            array_push($users, $this->toEntity($row));
        }
        return $users;
    }

    /**
     * @param $result
     * @return User
     */
    private function toEntity(array $result): User
    {
        return $this->hydrator->hydrate($result);
    }
}