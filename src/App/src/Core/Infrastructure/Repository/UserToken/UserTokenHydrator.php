<?php
declare(strict_types = 1);

namespace App\Core\Infrastructure\Repository\UserToken;

use App\Core\Domain\Model\UserToken\UserToken;
use App\Core\Infrastructure\Repository\Hydrator;
use function Zelenin\Hydrator\createObjectWithoutConstructor;
use Zelenin\Hydrator\NamingStrategy\UnderscoreToLowerCamelCaseNamingStrategy;
use Zelenin\Hydrator\Strategy\ReflectionStrategy;
use Zelenin\Hydrator\StrategyHydrator;
use DateTime;

final class UserTokenHydrator implements Hydrator
{
    /**
     * @param array $columns
     * @return UserToken
     */
    public function hydrate(array $columns): UserToken
    {
        $columns['created'] = new DateTime($columns['created']);
        $hydrator = new StrategyHydrator(new ReflectionStrategy(), new UnderscoreToLowerCamelCaseNamingStrategy());
        /* @var $object UserToken */
        $object = createObjectWithoutConstructor(UserToken::class);
        $object = $hydrator->hydrate($object, $columns);
        return $object;
    }
}