<?php
declare(strict_types=1);

namespace App\Core\Application\Event;

use App\Core\Application\Event\Middleware\RabbitMqMiddleware;
use Psr\Container\ContainerInterface;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Locator\HandlerResolver\ContainerHandlerResolver;
use Zelenin\MessageBus\Locator\Provider\MemoryProvider;
use Zelenin\MessageBus\Locator\ProviderLocator;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareBus;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareStack;

final class RabbitMqEventBus implements AsyncEventBus
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * BaseCommandBus constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $event
     * @return Context
     */
    public function handle(Event $event): Context
    {
        $handlers = [
            'App\Core\Application\Event\User\ConformingEmailRequested' => 'App\Core\Application\Event\User\ConformingEmailRequestedHandler',
            'App\Core\Application\Event\User\RecoveringPasswordRequested' => 'App\Core\Application\Event\User\RecoveringPasswordRequestedHandler',
            'App\Core\Application\Event\User\UserRegistered' => 'App\Core\Application\Event\User\UserRegisteredHandler',
        ];
        $handlersProvider = new MemoryProvider($handlers);
        $handlersLocator = new ProviderLocator($handlersProvider, new ContainerHandlerResolver($this->container));
        $eventBus = new MiddlewareBus(new MiddlewareStack([
            new RabbitMqMiddleware($handlersLocator),
        ]));
        return $eventBus->handle($event);
    }
}