<?php
declare(strict_types = 1);

namespace App\Core\Application\Event\User;

use App\Core\Domain\Model\UserToken\Types;
use App\Core\Domain\Model\UserToken\UserToken;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenRepository;
use App\Core\Domain\Service\Notification\User\UserEmailNotificator;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class UserRegisteredHandler implements Handler
{
    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    /**
     * @var UserEmailNotificator
     */
    private $userEmailNotificator;

    /**
     * EmailConformationRequestedHandler constructor.
     * @param UserReadRepository $userReadRepository
     * @param UserTokenRepository $userTokenRepository
     * @param UserEmailNotificator $userEmailNotificator
     */
    public function __construct(
        UserReadRepository $userReadRepository,
        UserTokenRepository $userTokenRepository,
        UserEmailNotificator $userEmailNotificator
    )
    {
        $this->userReadRepository = $userReadRepository;
        $this->userTokenRepository = $userTokenRepository;
        $this->userEmailNotificator = $userEmailNotificator;
    }

    /**
     * @param object $event
     * @param Context $context
     * @return Context
     */
    public function __invoke($event, Context $context): Context
    {
        $user = $this->userReadRepository->fetch($event->getUserId());
        $userTokenId = $this->userTokenRepository->nextIdentity();
        $userToken = UserToken::createNew(
            $userTokenId,
            $user->getId(),
            Types::EMAIL_CONFIRMATION
        );
        $this->userTokenRepository->add($userToken);
        $this->userEmailNotificator->sendEmailConformationEmail($userToken, $user);
        return $context;
    }
}