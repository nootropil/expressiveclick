<?php
declare(strict_types = 1);

namespace App\Core\Application\Event\User;

use App\Core\Application\Event\Event;

final class RecoveringPasswordRequested implements Event
{
    /**
     * @var string
     */
    private $userEmail;

    /**
     * PasswordRecoveringRequested constructor.
     * @param string $userEmail
     */
    public function __construct(string $userEmail)
    {
        $this->userEmail = $userEmail;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

}