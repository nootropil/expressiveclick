<?php
declare(strict_types = 1);

namespace App\Core\Application\Event;

use Zelenin\MessageBus\Context;

interface EventBus
{
    /**
     * @param Event $event
     * @return mixed
     */
    public function handle(Event $event) : Context;
}