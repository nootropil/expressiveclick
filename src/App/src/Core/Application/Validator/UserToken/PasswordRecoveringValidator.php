<?php
declare(strict_types=1);

namespace App\Core\Application\Validator\UserToken;

use App\Core\Application\Validator\ConstraintViolationsList;
use App\Core\Domain\Model\UserToken\Types;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;

final class PasswordRecoveringValidator
{
    const EXPIRE_TOKEN = 3600;

    /**
     * @var UserTokenReadRepository
     */
    private $userTokenReadRepository;

    /**
     * PasswordRecoveringValidator constructor.
     * @param UserTokenReadRepository $userTokenReadRepository
     */
    public function __construct(
        UserTokenReadRepository $userTokenReadRepository
    )
    {
        $this->userTokenReadRepository = $userTokenReadRepository;
    }

    /**
     * @param string $token
     * @return ConstraintViolationsList
     */
    public function validate(string $token): ConstraintViolationsList
    {
        $constrains = new ConstraintViolationsList();
        if (strlen($token) < 1) {
            $constrains->set('token', 'Токен некорректен');
        } elseif (!$this->userTokenReadRepository->existsByTokenAndType($token, Types::PASSWORD_RECOVERING)) {
            $constrains->set('token', 'Токена не существует');
        } elseif ($constrains->isEmpty()) {
            $userToken = $this->userTokenReadRepository->fetchByTokenAndType($token, Types::PASSWORD_RECOVERING);
            if ($userToken->getCreated()->getTimestamp() + self::EXPIRE_TOKEN <= (new \DateTime('now'))->getTimestamp()) {
                $constrains->set('token', 'Токен просрочен');
            }
        }
        return $constrains;
    }
}