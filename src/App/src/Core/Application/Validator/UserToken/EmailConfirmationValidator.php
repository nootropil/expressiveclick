<?php
declare(strict_types = 1);

namespace App\Core\Application\Validator\UserToken;

use App\Core\Application\Validator\ConstraintViolationsList;
use App\Core\Domain\Model\UserToken\Types;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;

final class EmailConfirmationValidator
{
    const EXPIRE_TOKEN = 3600;

    /**
     * @var UserReadRepository
     */
    private $userReadRepository;
    /**
     * @var UserTokenReadRepository
     */
    private $userTokenReadRepository;

    /**
     * EmailConfirmationValidator constructor.
     * @param UserReadRepository $userReadRepository
     * @param UserTokenReadRepository $userTokenReadRepository
     */
    public function __construct(
        UserReadRepository $userReadRepository,
        UserTokenReadRepository $userTokenReadRepository
    )
    {
        $this->userReadRepository = $userReadRepository;
        $this->userTokenReadRepository = $userTokenReadRepository;
    }

    /**
     * @param string $token
     * @return ConstraintViolationsList
     */
    public function validate(string $token): ConstraintViolationsList
    {
        $constrains = new ConstraintViolationsList();
        if (strlen($token) < 1) {
            $constrains->set('token', 'Токен некорректен');
        } elseif (!$this->userTokenReadRepository->existsByTokenAndType($token, Types::EMAIL_CONFIRMATION)) {
            $constrains->set('token', 'Токена не существует');
        } elseif ($constrains->isEmpty()) {
            $userToken = $this->userTokenReadRepository->fetchByTokenAndType($token, Types::EMAIL_CONFIRMATION);
            $user = $this->userReadRepository->fetch($userToken->getUserId());
            if ($userToken->getCreated()->getTimestamp() + self::EXPIRE_TOKEN <= (new \DateTime('now'))->getTimestamp()) {
                $constrains->set('token', 'Токен просрочен');
            } elseif (!$user->isUnconfirmedUser()) {
                $constrains->set('token', 'Email уже подтверждён');
            }
        }
        return $constrains;
    }
}