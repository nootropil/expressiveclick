<?php
declare(strict_types = 1);

namespace App\Core\Application\Validator;


final class ConstraintViolationsList
{
    /**
     * @var array
     */
    private $violations = [];

    /**
     * @param array $violations
     */
    public function __construct(array $violations = [])
    {
        $this->violations = $violations;
    }

    /**
     * @param $violation
     */
    public function add($violation)
    {
        $this->violations[] = $violation;
    }

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        $this->violations[$key] = $value;
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    public function remove($key)
    {
        if (!$this->containsKey($key)) {
            return null;
        }

        $violation = $this->violations[$key];
        unset($this->violations[$key]);

        return $violation;
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    public function get($key)
    {
        return $this->containsKey($key) ? $this->violations[$key] : null;
    }

    public function clear()
    {
        $this->violations = [];
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->count() === 0;
    }

    /**
     * @return array
     */
    public function getKeys()
    {
        return array_keys($this->violations);
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return array_values($this->violations);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->violations);
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function containsKey($key)
    {
        return array_key_exists($key, $this->violations);
    }
}