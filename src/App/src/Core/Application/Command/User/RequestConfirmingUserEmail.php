<?php
declare(strict_types = 1);

namespace App\Core\Application\Command\User;

use App\Core\Application\Command\Command;

final class RequestConfirmingUserEmail implements Command
{
    /**
     * @var string
     */
    private $email;

    /**
     * RequestConfirmingUserEmail constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}