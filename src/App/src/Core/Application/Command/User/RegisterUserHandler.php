<?php
declare(strict_types=1);

namespace App\Core\Application\Command\User;

use App\Core\Application\Event\AsyncEventBus;
use App\Core\Application\Event\User\UserRegistered;
use App\Core\Domain\Model\User\User;
use App\Core\Domain\Repository\User\UserRepository;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class RegisterUserHandler implements Handler
{
    /**
     * @var UserRepository
     */
    private $userRepository;


    /**
     * @var AsyncEventBus
     */
    private $asyncEventBus;

    /**
     * RegisterUserHandler constructor.
     * @param UserRepository $userRepository
     * @param AsyncEventBus $asyncEventBus
     */
    public function __construct(
        UserRepository $userRepository,
        AsyncEventBus $asyncEventBus
    )
    {
        $this->userRepository = $userRepository;
        $this->asyncEventBus = $asyncEventBus;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     */
    public function __invoke($command, Context $context): Context
    {
        $id = $this->userRepository->nextIdentity();
        $user = User::registerNew(
            $id,
            $command->getUsername(),
            $command->getEmail(),
            $command->getPassword()
        );
        $this->userRepository->add($user);
        $this->asyncEventBus->handle(new UserRegistered($user->getId()));
        return $context;
    }
}