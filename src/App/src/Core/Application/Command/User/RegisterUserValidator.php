<?php
declare(strict_types=1);

namespace App\Core\Application\Command\User;

use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Infrastructure\Exception\ValidationException;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class RegisterUserValidator implements Handler
{
    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * RegisterUserValidator constructor.
     * @param UserReadRepository $userReadRepository
     */
    public function __construct(UserReadRepository $userReadRepository)
    {
        $this->userReadRepository = $userReadRepository;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     * @throws ValidationException
     */
    public function __invoke($command, Context $context): Context
    {
        /* @var $command RegisterUser */
        if ($this->userReadRepository->existsByUsername($command->getUsername())) {
            throw new ValidationException('Этот логин уже зарегестрирован.');
        }

        if ($this->userReadRepository->existsByEmail($command->getEmail())) {
            throw new ValidationException('Этот emil уже зарегестрирован.');
        }
        return $context;
    }
}