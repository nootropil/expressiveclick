<?php
declare(strict_types=1);

namespace App\Core\Application\Command\User;

use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Infrastructure\Exception\ValidationException;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class RequestConfirmingUserEmailValidator implements Handler
{
    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * RequestConfirmingUserEmailValidator constructor.
     * @param UserReadRepository $userReadRepository
     */
    public function __construct(UserReadRepository $userReadRepository)
    {
        $this->userReadRepository = $userReadRepository;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     * @throws ValidationException
     */
    public function __invoke($command, Context $context): Context
    {
        if (!$this->userReadRepository->existsByEmail($command->getEmail())) {
            throw new ValidationException('Email не существует');
        }
        return $context;
    }
}