<?php
declare(strict_types=1);

namespace App\Core\Application\Command\User;

use App\Core\Application\Validator\UserToken\PasswordRecoveringValidator;
use App\Core\Infrastructure\Exception\ValidationException;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class RecoverUserPasswordValidator implements Handler
{
    private $validator;

    /**
     * RecoverUserPasswordValidator constructor.
     * @param PasswordRecoveringValidator $validator
     */
    public function __construct(PasswordRecoveringValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     * @throws ValidationException
     */
    public function __invoke($command, Context $context): Context
    {
        /* @var $command ConfirmUserEmail */
        $violations = $this->validator->validate($command->getToken());
        if (!$violations->isEmpty()) {
            throw new ValidationException($violations->get('token'));
        }
        return $context;
    }
}