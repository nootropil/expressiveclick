<?php
declare(strict_types=1);

namespace App\Core\Application\Command\User;

use App\Core\Application\Event\AsyncEventBus;
use App\Core\Application\Event\User\ConformingEmailRequested;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class RequestConfirmingUserEmailHandler implements Handler
{
    /**
     * @var AsyncEventBus
     */
    private $asyncEventBus;

    /**
     * RequestConfirmingUserEmailHandler constructor.
     * @param AsyncEventBus $asyncEventBus
     */
    public function __construct(
        AsyncEventBus $asyncEventBus
    )
    {
        $this->asyncEventBus = $asyncEventBus;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     */
    public function __invoke($command, Context $context): Context
    {
        /* @var $command RequestConfirmingUserEmail */
        $this->asyncEventBus->handle(new ConformingEmailRequested($command->getEmail()));
        return $context;
    }
}