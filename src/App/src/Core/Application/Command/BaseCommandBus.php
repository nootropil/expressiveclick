<?php
declare(strict_types = 1);

namespace App\Core\Application\Command;

use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Locator\HandlerResolver\ContainerHandlerResolver;
use Zelenin\MessageBus\Locator\Provider\MemoryProvider;
use Zelenin\MessageBus\Locator\ProviderLocator;
use Zelenin\MessageBus\MiddlewareBus\Middleware\HandlerMiddleware;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareBus;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareStack;
use Interop\Container\ContainerInterface;

final class BaseCommandBus implements CommandBus
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * BaseCommandBus constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $command
     * @return Context
     */
    public function handle(Command $command): Context
    {
        $handlers = [
            'App\Core\Application\Command\User\RegisterUser' => 'App\Core\Application\Command\User\RegisterUserHandler',
            'App\Core\Application\Command\User\ConfirmUserEmail' => 'App\Core\Application\Command\User\ConfirmUserEmailHandler',
            'App\Core\Application\Command\User\RecoverUserPassword' => 'App\Core\Application\Command\User\RecoverUserPasswordHandler',
            'App\Core\Application\Command\User\RequestConfirmingUserEmail' => 'App\Core\Application\Command\User\RequestConfirmingUserEmailHandler',
            'App\Core\Application\Command\User\RequestRecoveringUserPassword' => 'App\Core\Application\Command\User\RequestRecoveringUserPasswordHandler',
        ];
        $validators = [
            'App\Core\Application\Command\User\RegisterUser' => 'App\Core\Application\Command\User\RegisterUserValidator',
            'App\Core\Application\Command\User\ConfirmUserEmail' => 'App\Core\Application\Command\User\ConfirmUserEmailValidator',
            'App\Core\Application\Command\User\RecoverUserPassword'  =>  'App\Core\Application\Command\User\RecoverUserPasswordValidator',
            'App\Core\Application\Command\User\RequestConfirmingUserEmail' => 'App\Core\Application\Command\User\RequestConfirmingUserEmailValidator',
            'App\Core\Application\Command\User\RequestRecoveringUserPassword' => 'App\Core\Application\Command\User\RequestRecoveringUserPasswordValidator',
        ];

        $handlersProvider = new MemoryProvider($handlers);
        $validatorsProvider = new MemoryProvider($validators);

        $handlersLocator = new ProviderLocator($handlersProvider, new ContainerHandlerResolver($this->container));
        $validatorsLocator = new ProviderLocator($validatorsProvider, new ContainerHandlerResolver($this->container));

        $commandBus = new MiddlewareBus(new MiddlewareStack([
            new HandlerMiddleware($validatorsLocator),
            new HandlerMiddleware($handlersLocator),
        ]));
        return $commandBus->handle($command);
    }
}