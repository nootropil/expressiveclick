<?php
declare(strict_types = 1);

namespace App\Core\Domain\Model\UserToken;

final class Types
{
    const EMAIL_CONFIRMATION = 'email-confirmation';
    const PASSWORD_RECOVERING = 'password-recovering';
}