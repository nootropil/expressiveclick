<?php
declare(strict_types = 1);

namespace App\Core\Domain\Repository\UserToken;

use App\Core\Domain\Model\UserToken\UserToken;

interface UserTokenReadRepository
{
    /**
     * @param $id
     * @return UserToken|null
     */
    public function fetch(string $id): ?UserToken;

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool;

    /**
     * @param string $token
     * @param string $type
     * @return UserToken|null
     */
    public function fetchByTokenAndType(string $token, string $type): ?UserToken;

    /**
     * @param string $token
     * @param string $type
     * @return bool
     */
    public function existsByTokenAndType(string $token, string $type): bool;

}