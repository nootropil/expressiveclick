<?php
namespace App\Command;

use App\Core\Application\Event\EventBus;
use App\Core\Application\Service\Transformer\EventTransformer;
use App\Core\Infrastructure\Notification\ConnectionFabric;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListenEventCommand extends Command
{
    /**
     * @var EventBus
     */
    private $eventBus;

    /**
     * @var EventTransformer
     */
    private $eventTransformer;

    /**
     * ListenEventCommand constructor.
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventTransformer = new EventTransformer();
        $this->eventBus = $eventBus;
        parent::__construct();
    }

    /**
     * Configures the command
     */
    protected function configure()
    {
        $this
            ->setName('greet')
            ->setDescription('Greet someone');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $bindingKeys = ['app.core.application.event.#'];

        $connection = ConnectionFabric::createConnection();
        $channel = $connection->channel();
        $channel->exchange_declare('app_events', 'topic', false, false, false);
        list($queueName, ,) = $channel->queue_declare("", false, false, true, false);
        foreach($bindingKeys as $bindingKey) {
            $channel->queue_bind($queueName, 'app_events', $bindingKey);
        }
        $output->writeln(' [*] Waiting for events. To exit press CTRL+C', "\n");
        $callback = function($msg){
            $data = json_decode($msg->body, true);
            try {
                $this->eventBus->handle($this->eventTransformer->fromArray($data['message']));
            } catch (\Exception $exception) {
                echo $exception->getMessage();
            }
        };
        $channel->basic_consume($queueName, '', false, true, false, false, $callback);
        while(count($channel->callbacks)) {
            $channel->wait();
        }
        $channel->close();
        $connection->close();
    }
}