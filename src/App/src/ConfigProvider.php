<?php
declare(strict_types=1);

namespace App;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
final class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
            'console' => $this->getConsole(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'factories' => [
                Core\Application\Command\CommandBus::class => Factory\Core\Application\Command\CommandBusFactory::class,
                Core\Application\Event\AsyncEventBus::class => Factory\Core\Application\Event\AsyncEventBusFactory::class,
                Core\Application\Event\EventBus::class => Factory\Core\Application\Event\EventBusFactory::class,

                /* Repositories */
                Core\Domain\Repository\User\UserReadRepository::class => Factory\Core\Domain\Repository\User\UserReadRepositoryFactory::class,
                Core\Domain\Repository\User\UserRepository::class => Factory\Core\Domain\Repository\User\UserRepositoryFactory::class,
                Core\Domain\Repository\UserToken\UserTokenReadRepository::class => Factory\Core\Domain\Repository\UserToken\UserTokenReadRepositoryFactory::class,
                Core\Domain\Repository\UserToken\UserTokenRepository::class => Factory\Core\Domain\Repository\UserToken\UserTokenRepositoryFactory::class,


                /* Commands */
                Core\Application\Command\User\ConfirmUserEmailHandler::class => Factory\Core\Application\Command\User\ConfirmUserEmailHandlerFactory::class,
                Core\Application\Command\User\ConfirmUserEmailValidator::class => Factory\Core\Application\Command\User\ConfirmUserEmailValidatorFactory::class,
                Core\Application\Command\User\RecoverUserPasswordHandler::class => Factory\Core\Application\Command\User\RecoverUserPasswordHandlerFactory::class,
                Core\Application\Command\User\RecoverUserPasswordValidator::class => Factory\Core\Application\Command\User\RecoverUserPasswordValidatorFactory::class,
                Core\Application\Command\User\RegisterUserHandler::class => Factory\Core\Application\Command\User\RegisterUserHandlerFactory::class,
                Core\Application\Command\User\RegisterUserValidator::class => Factory\Core\Application\Command\User\RegisterUserValidatorFactory::class,
                Core\Application\Command\User\RequestRecoveringUserPasswordValidator::class => Factory\Core\Application\Command\User\RequestRecoveringUserPasswordValidatorFactory::class,
                Core\Application\Command\User\RequestRecoveringUserPasswordHandler::class => Factory\Core\Application\Command\User\RequestRecoveringUserPasswordHandlerFactory::class,
                Core\Application\Command\User\RequestConfirmingUserEmailValidator::class => Factory\Core\Application\Command\User\RequestConfirmingUserEmailValidatorFactory::class,
                Core\Application\Command\User\RequestConfirmingUserEmailHandler::class => Factory\Core\Application\Command\User\RequestConfirmingUserEmailHandlerFactory::class,


                /* Events */
                Core\Application\Event\User\UserRegisteredHandler::class => Factory\Core\Application\Event\User\UserRegisteredHandlerFactory::class,
                Core\Application\Event\User\RecoveringPasswordRequestedHandler::class => Factory\Core\Application\Event\User\RecoveringPasswordRequestedHandlerFactory::class,
                Core\Application\Event\User\ConformingEmailRequestedHandler::class => Factory\Core\Application\Event\User\ConformingEmailRequestedHandlerFactory::class,

                /* Actions */
                Presentation\Action\User\RegistrationAction::class => Factory\Presentation\Action\User\RegistrationActionFactory::class,
                Presentation\Action\User\EmailConfirmationAction::class => Factory\Presentation\Action\User\EmailConfirmationActionFactory::class,
                Presentation\Action\User\EmailConfirmationRequestAction::class => Factory\Presentation\Action\User\EmailConfirmationRequestActionFactory::class,
                Presentation\Action\User\PasswordRecoveringAction::class => Factory\Presentation\Action\User\PasswordRecoveringActionFactory::class,
                Presentation\Action\User\PasswordRecoveringRequestAction::class => Factory\Presentation\Action\User\PasswordRecoveringRequestActionFactory::class,
                Presentation\Action\Common\IndexAction::class => Factory\Presentation\Action\Common\IndexActionFactory::class,

                /* Forms */
                Presentation\Form\User\RegistrationForm::class => Factory\Presentation\Form\User\RegistrationFormFactory::class,
                Presentation\Form\User\EmailConformationRequestForm::class => Factory\Presentation\Form\User\EmailConformationRequestFormFactory::class,
                Presentation\Form\User\PasswordRecoveringForm::class => Factory\Presentation\Form\User\PasswordRecoveringFormFactory::class,
                Presentation\Form\User\PasswordRecoveringRequestForm::class => Factory\Presentation\Form\User\PasswordRecoveringRequestFormFactory::class,

                /* Services */
                Core\Domain\Service\Notification\User\UserEmailNotificator::class => Factory\Core\Domain\Service\Notification\User\UserEmailNotificatorFactory::class,

                /* Validators */
                Core\Application\Validator\UserToken\EmailConfirmationValidator::class => Factory\Core\Application\Validator\UserToken\EmailConfirmationValidatorFactory::class,
                Core\Application\Validator\UserToken\PasswordRecoveringValidator::class => Factory\Core\Application\Validator\UserToken\PasswordRecoveringValidatorFactory::class,


                /*Console commands */

                Command\ListenEventCommand::class => Factory\Command\ListenEventCommandFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'email' => ['templates/email'],
                'app' => ['src/App/templates'],
                'error' => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }

    public function getConsole()
    {
        return [
            'commands' => [
                Command\ListenEventCommand::class,
            ],
        ];
    }
}
