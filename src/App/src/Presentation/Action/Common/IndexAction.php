<?php
declare(strict_types=1);

namespace App\Presentation\Action\Common;


use App\Core\Domain\Repository\User\UserReadRepository;
use Aura\Session\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;
use Zend\Expressive\Template\TemplateRendererInterface;

class IndexAction
{

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * @var UserReadRepository
     */
    private $userReadRepository;


    /**
     * IndexAction constructor.
     * @param TemplateRendererInterface $template
     * @param UserReadRepository $userReadRepository
     */
    public function __construct(TemplateRendererInterface $template, UserReadRepository $userReadRepository)
    {
        $this->template = $template;
        $this->userReadRepository = $userReadRepository;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return HtmlResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        /**
         * @var Session $session
         */
        $session = $request->getAttribute('session');
        $flashMessage = $session->getSegment('App\Action')->getFlash('flash');
        $users = $this->userReadRepository->fetchAll();
        return new HtmlResponse($this->template->render('app::default/index', compact('flashMessage', 'users')));
    }
}
