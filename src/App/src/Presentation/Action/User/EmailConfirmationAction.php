<?php
declare(strict_types=1);

namespace App\Presentation\Action\User;

use App\Core\Application\Command\User\ConfirmUserEmail;
use App\Core\Infrastructure\Exception\ValidationException;
use Aura\Session\Session;
use App\Core\Application\Command\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Whoops\Exception\ErrorException;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;

class EmailConfirmationAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Router\RouterInterface
     */
    protected $router;


    /**
     * EmailConfirmationAction constructor.
     * @param Router\RouterInterface $router
     * @param CommandBus $commandBus
     */
    public function __construct(
        Router\RouterInterface $router,
        CommandBus $commandBus
    )
    {

        $this->commandBus = $commandBus;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return RedirectResponse
     * @throws ErrorException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        /**
         * @var Session $session
         */
        $session = $request->getAttribute('session');
        $token = $request->getAttribute('token');
        try {
            $this->commandBus->handle(new ConfirmUserEmail($token));
            $session->getSegment('App\Action')->setFlash(
                'flash',
                ['type' => 'success', 'message' => 'Вы успешно зарегестрированны']
            );
            return new RedirectResponse($this->router->generateUri('index'));
        } catch (ValidationException $exception) {
            throw new ErrorException($exception->getMessage());
        }
    }
}
