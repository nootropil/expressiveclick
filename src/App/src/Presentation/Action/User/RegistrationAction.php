<?php
declare(strict_types=1);

namespace App\Presentation\Action\User;

use App\Core\Application\Command\User\RegisterUser;
use App\Presentation\Form\User\RegistrationForm;
use Aura\Session\Session;
use App\Core\Application\Command\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;

class RegistrationAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Router\RouterInterface
     */
    protected $router;

    /**
     * @var RegistrationForm
     */
    protected $form;

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * RegistrationAction constructor.
     * @param Router\RouterInterface $router
     * @param Template\TemplateRendererInterface|null $template
     * @param RegistrationForm $form
     * @param CommandBus $commandBus
     */
    public function __construct(
        Router\RouterInterface $router,
        Template\TemplateRendererInterface $template = null,
        RegistrationForm $form,
        CommandBus $commandBus
    )
    {

        $this->commandBus = $commandBus;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        /**
         * @var Session $session
         */
        $session = $request->getAttribute('session');
        $this->form->setData($request->getParsedBody());

        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->commandBus->handle(new RegisterUser($this->form->getData()));
            $session->getSegment('App\Action')->setFlash(
                'flash',
                ['type' => 'success', 'message' => 'Вы успешно зарегестрированны']
            );
            return new RedirectResponse($this->router->generateUri('index'));
        }
        return new HtmlResponse($this->template->render('app::user/registration', [
            'form' => $this->form,
        ]));
    }
}
