<?php
declare(strict_types=1);

namespace App\Presentation\Action\User;

use App\Core\Application\Command\User\RecoverUserPassword;
use App\Core\Application\Validator\UserToken\PasswordRecoveringValidator;
use App\Presentation\Form\User\PasswordRecoveringForm;
use Aura\Session\Session;
use App\Core\Application\Command\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;

class PasswordRecoveringAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Router\RouterInterface
     */
    protected $router;

    /**
     * @var PasswordRecoveringForm
     */
    protected $form;

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * @var PasswordRecoveringValidator
     */
    private $validator;

    /**
     * PasswordRecoveringAction constructor.
     * @param Router\RouterInterface $router
     * @param Template\TemplateRendererInterface|null $template
     * @param PasswordRecoveringForm $form
     * @param CommandBus $commandBus
     * @param PasswordRecoveringValidator $validator
     */
    public function __construct(
        Router\RouterInterface $router,
        Template\TemplateRendererInterface $template = null,
        PasswordRecoveringForm $form,
        CommandBus $commandBus,
        PasswordRecoveringValidator $validator
    )
    {
        $this->validator = $validator;
        $this->commandBus = $commandBus;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        /**
         * @var Session $session
         */
        $session = $request->getAttribute('session');
        $violations = $this->validator->validate($request->getAttribute('token'));
        if (!$violations->isEmpty()) {
            $session->getSegment('App\Action')->setFlash(
                'flash',
                ['type' => 'error', 'message' => $violations->get('token')]
            );
            return new RedirectResponse($this->router->generateUri('index'));
        }


        $data = $request->getParsedBody();
        $this->form->setData($data);
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->commandBus->handle(new RecoverUserPassword($request->getAttribute('token'), $data['password']));
            $session->getSegment('App\Action')->setFlash(
                'flash',
                ['type' => 'success', 'message' => 'Пароль изменён']
            );
            return new RedirectResponse($this->router->generateUri('index'));
        }
        return new HtmlResponse($this->template->render('app::user/password-recovering', [
            'form' => $this->form,
        ]));
    }
}
