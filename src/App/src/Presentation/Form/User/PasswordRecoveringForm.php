<?php
declare(strict_types=1);

namespace App\Presentation\Form\User;

use Aura\Session\CsrfToken;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

final class PasswordRecoveringForm extends Form implements InputFilterProviderInterface
{
    /**
     * @param string|null $name
     * @param array|null $options
     */
    public function __construct($name = null, $options = [])
    {
        // we want to ignore the name passed
        parent::__construct('password-recovering', $options);

        $this->add([
            'name' => 'password',
            'type' => 'Text',
            'options' => [
                'label' => 'Password',
            ],
        ]);

        $this->add([
            'name' => 'passwordRepeat',
            'type' => 'Text',
            'options' => [
                'label' => 'Password Repeat',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Register',
                'id' => 'submit-button',
            ],
        ]);

        $this->add([
            'name' => '_csrf',
            'type' => 'hidden',
            'attributes' => [
                'value' => $this->getOption('csrf')->getValue(),
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'password',
                'required' => true,
                'error_message' => ' passwordThe reason must be between 10 and 150 characters in length.',
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                ],
            ],

            [
                'name' => 'passwordRepeat',
                'required' => true,
                'error_message' => 'passwordRepeat The reason must be between 10 and 150 characters in length.',
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                ],
            ],


            [
                'name' => '_csrf',
                'require' => true,
                'validators' => [
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context, CsrfToken $csrf) {
                                if ($csrf->isValid($value)) {
                                    return true;
                                }

                                return false;
                            },
                            'callbackOptions' => [
                                $this->getOption('csrf'),
                            ],
                            'message' => 'The form submitted did not originate from the expected site',
                        ],
                    ],
                ],
            ],
        ];
    }
}
