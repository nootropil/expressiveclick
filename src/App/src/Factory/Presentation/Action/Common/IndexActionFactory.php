<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\Common;

use App\Core\Domain\Repository\User\UserReadRepository;
use App\Presentation\Action\Common\IndexAction;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\I18n\Translator\Translator;

final class IndexActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return IndexAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $template = $container->get(TemplateRendererInterface::class);
        $userReadRepository = $container->get(UserReadRepository::class);
        $translator= $container->get(Translator::class);
        return new IndexAction($template, $userReadRepository, $translator);
    }
}
