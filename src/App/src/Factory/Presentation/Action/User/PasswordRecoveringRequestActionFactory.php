<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\User;

use App\Presentation\Action\User\PasswordRecoveringRequestAction;
use App\Core\Application\Command\CommandBus;
use App\Presentation\Form\User\PasswordRecoveringRequestForm;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class PasswordRecoveringRequestActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return PasswordRecoveringRequestAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);
        $template = ($container->has(TemplateRendererInterface::class))
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $form = $container->get(PasswordRecoveringRequestForm::class);
        $commandBus = $container->get(CommandBus::class);

        return new PasswordRecoveringRequestAction($router, $template, $form, $commandBus);
    }
}
