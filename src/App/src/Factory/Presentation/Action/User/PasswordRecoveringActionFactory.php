<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\User;

use App\Core\Application\Validator\UserToken\PasswordRecoveringValidator;
use App\Presentation\Action\User\PasswordRecoveringAction;
use App\Presentation\Form\User\PasswordRecoveringForm;
use App\Core\Application\Command\CommandBus;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class PasswordRecoveringActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return PasswordRecoveringAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);
        $template = ($container->has(TemplateRendererInterface::class))
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $form = $container->get(PasswordRecoveringForm::class);
        $commandBus = $container->get(CommandBus::class);
        $validator = $container->get(PasswordRecoveringValidator::class);

        return new PasswordRecoveringAction($router, $template, $form, $commandBus, $validator);
    }
}
