<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\User;

use App\Presentation\Action\User\EmailConfirmationAction;
use App\Core\Application\Command\CommandBus;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

final class EmailConfirmationActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return EmailConfirmationAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);
        $commandBus = $container->get(CommandBus::class);
        return new EmailConfirmationAction($router, $commandBus);
    }
}
