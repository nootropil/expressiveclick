<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\User;

use App\Presentation\Action\User\EmailConfirmationRequestAction;
use App\Presentation\Form\User\EmailConformationRequestForm;
use App\Core\Application\Command\CommandBus;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class EmailConfirmationRequestActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return EmailConfirmationRequestAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);
        $template = ($container->has(TemplateRendererInterface::class))
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $form = $container->get(EmailConformationRequestForm::class);
        $commandBus = $container->get(CommandBus::class);

        return new EmailConfirmationRequestAction($router, $template, $form, $commandBus);
    }
}
