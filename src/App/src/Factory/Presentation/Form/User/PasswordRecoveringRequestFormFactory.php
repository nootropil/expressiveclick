<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Form\User;

use App\Presentation\Form\User\PasswordRecoveringRequestForm;
use Aura\Session\Session;
use Interop\Container\ContainerInterface;

final class PasswordRecoveringRequestFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return PasswordRecoveringRequestForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);

        return new PasswordRecoveringRequestForm(null, ['csrf' => $session->getCsrfToken()]);
    }
}