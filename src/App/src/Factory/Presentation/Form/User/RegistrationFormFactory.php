<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Form\User;

use App\Presentation\Form\User\RegistrationForm;
use Aura\Session\Session;
use Interop\Container\ContainerInterface;

final class RegistrationFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return RegistrationForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);

        return new RegistrationForm('registration', ['csrf' => $session->getCsrfToken()]);
    }
}