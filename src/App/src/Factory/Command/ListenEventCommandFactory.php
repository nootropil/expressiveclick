<?php
declare(strict_types = 1);

namespace App\Factory\Command;

use App\Command\ListenEventCommand;
use App\Core\Application\Event\EventBus;
use App\Core\Application\Service\Transformer\EventTransformer;
use Interop\Container\ContainerInterface;

class ListenEventCommandFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $eventBus = $container->get(EventBus::class);
        return new ListenEventCommand($eventBus);
    }
}