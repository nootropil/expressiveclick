<?php
declare(strict_types = 1);

namespace App\Factory\Core\Domain\Service\Notification\User;


use App\Core\Application\Service\Notification\User\SwiftMailerUserEmailNotificator;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class UserEmailNotificatorFactory
{
    /**
     * @param ContainerInterface $container
     * @return SwiftMailerUserEmailNotificator
     */
    public function __invoke(ContainerInterface $container)
    {
        $template = $container->get(TemplateRendererInterface::class);
        return new SwiftMailerUserEmailNotificator($template);
    }
}