<?php
declare(strict_types=1);

namespace App\Factory\Core\Domain\Repository\UserToken;

use App\Core\Infrastructure\Repository\UserToken\PdoUserTokenReadRepository;
use Interop\Container\ContainerInterface;
use PDO;

final class UserTokenReadRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return PdoUserTokenReadRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $connectionConfig = $config['db_connection'];
        $pdo = new PDO($connectionConfig);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return new PdoUserTokenReadRepository($pdo);
    }
}