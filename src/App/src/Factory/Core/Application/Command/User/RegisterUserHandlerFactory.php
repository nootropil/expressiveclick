<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command\User;

use App\Core\Application\Command\User\RegisterUserHandler;
use App\Core\Application\Event\AsyncEventBus;
use App\Core\Domain\Repository\User\UserRepository;
use Interop\Container\ContainerInterface;

final class RegisterUserHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return RegisterUserHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        $userRepository = $container->get(UserRepository::class);
        $asyncEventBus = $container->get(AsyncEventBus::class);
        return new RegisterUserHandler($userRepository, $asyncEventBus);
    }
}