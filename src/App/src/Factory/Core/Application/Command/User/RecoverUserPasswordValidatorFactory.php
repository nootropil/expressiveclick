<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command\User;

use App\Core\Application\Command\User\RecoverUserPasswordValidator;
use App\Core\Application\Validator\UserToken\PasswordRecoveringValidator;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use Interop\Container\ContainerInterface;

final class RecoverUserPasswordValidatorFactory
{
    /**
     * @param ContainerInterface $container
     * @return RecoverUserPasswordValidator
     */
    public function __invoke(ContainerInterface $container)
    {
        $validator = $container->get(PasswordRecoveringValidator::class);
        return new RecoverUserPasswordValidator($validator);
    }
}