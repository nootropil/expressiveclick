<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command\User;

use App\Core\Application\Command\User\RegisterUserValidator;
use App\Core\Domain\Repository\User\UserReadRepository;
use Interop\Container\ContainerInterface;

final class RegisterUserValidatorFactory
{
    /**
     * @param ContainerInterface $container
     * @return RegisterUserValidator
     */
    public function __invoke(ContainerInterface $container)
    {
        $userReadRepository = $container->get(UserReadRepository::class);
        return new RegisterUserValidator($userReadRepository);
    }
}