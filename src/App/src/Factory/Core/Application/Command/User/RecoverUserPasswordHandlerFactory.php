<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command\User;

use App\Core\Application\Command\User\RecoverUserPasswordHandler;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\User\UserRepository;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenRepository;
use Interop\Container\ContainerInterface;

final class RecoverUserPasswordHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return RecoverUserPasswordHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        $userRepository = $container->get(UserRepository::class);
        $userReadRepository = $container->get(UserReadRepository::class);
        $userTokenRepository = $container->get(UserTokenRepository::class);
        $userTokenReadRepository = $container->get(UserTokenReadRepository::class);
        return new RecoverUserPasswordHandler($userRepository, $userReadRepository, $userTokenReadRepository, $userTokenRepository);
    }
}