<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command\User;

use App\Core\Application\Command\User\RequestRecoveringUserPasswordValidator;
use App\Core\Domain\Repository\User\UserReadRepository;
use Interop\Container\ContainerInterface;

final class RequestRecoveringUserPasswordValidatorFactory
{
    /**
     * @param ContainerInterface $container
     * @return RequestRecoveringUserPasswordValidator
     */
    public function __invoke(ContainerInterface $container)
    {
        $userReadRepository = $container->get(UserReadRepository::class);
        return new RequestRecoveringUserPasswordValidator($userReadRepository);
    }
}