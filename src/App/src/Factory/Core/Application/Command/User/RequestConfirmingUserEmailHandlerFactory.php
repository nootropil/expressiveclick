<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command\User;

use App\Core\Application\Command\User\RequestConfirmingUserEmailHandler;
use App\Core\Application\Event\AsyncEventBus;
use Interop\Container\ContainerInterface;

final class RequestConfirmingUserEmailHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return RequestConfirmingUserEmailHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        $asyncEventBus = $container->get(AsyncEventBus::class);
        return new RequestConfirmingUserEmailHandler($asyncEventBus);
    }
}