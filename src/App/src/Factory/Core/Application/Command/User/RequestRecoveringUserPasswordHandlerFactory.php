<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command\User;

use App\Core\Application\Command\User\RequestRecoveringUserPasswordHandler;
use App\Core\Application\Event\AsyncEventBus;
use Interop\Container\ContainerInterface;

final class RequestRecoveringUserPasswordHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return RequestRecoveringUserPasswordHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        $asyncEventBus = $container->get(AsyncEventBus::class);
        return new RequestRecoveringUserPasswordHandler($asyncEventBus);
    }
}