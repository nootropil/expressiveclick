<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command\User;

use App\Core\Application\Command\User\ConfirmUserEmailValidator;
use App\Core\Application\Validator\UserToken\EmailConfirmationValidator;
use Interop\Container\ContainerInterface;

final class ConfirmUserEmailValidatorFactory
{
    /**
     * @param ContainerInterface $container
     * @return ConfirmUserEmailValidator
     */
    public function __invoke(ContainerInterface $container)
    {
        $validator = $container->get(EmailConfirmationValidator::class);
        return new ConfirmUserEmailValidator($validator);
    }
}