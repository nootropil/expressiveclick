<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Command;

use App\Core\Application\Command\BaseCommandBus;
use Interop\Container\ContainerInterface;

final class CommandBusFactory
{
    /**
     * @param ContainerInterface $container
     * @return BaseCommandBus
     */
    public function __invoke(ContainerInterface $container)
    {
        return new BaseCommandBus($container);
    }
}