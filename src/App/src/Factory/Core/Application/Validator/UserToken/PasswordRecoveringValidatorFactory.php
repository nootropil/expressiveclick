<?php
declare(strict_types=1);

namespace App\Factory\Core\Application\Validator\UserToken;

use App\Core\Application\Validator\UserToken\PasswordRecoveringValidator;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use Psr\Container\ContainerInterface;


final class PasswordRecoveringValidatorFactory
{
    /**
     * @param ContainerInterface $container
     * @return PasswordRecoveringValidator
     */
    public function __invoke(ContainerInterface $container)
    {
        $userTokenRepository = $container->get(UserTokenReadRepository::class);
        return new PasswordRecoveringValidator($userTokenRepository);
    }
}