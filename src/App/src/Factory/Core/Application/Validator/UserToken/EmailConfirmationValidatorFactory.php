<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Validator\UserToken;

use App\Core\Application\Validator\UserToken\EmailConfirmationValidator;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use Psr\Container\ContainerInterface;

final class EmailConfirmationValidatorFactory
{
    /**
     * @param ContainerInterface $container
     * @return EmailConfirmationValidator
     */
    public function __invoke(ContainerInterface $container)
    {
        $userReadRepository = $container->get(UserReadRepository::class);
        $userTokenRepository = $container->get(UserTokenReadRepository::class);
        return new EmailConfirmationValidator($userReadRepository, $userTokenRepository);
    }
}