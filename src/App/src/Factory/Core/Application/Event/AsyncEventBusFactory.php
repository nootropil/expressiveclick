<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Event;

use App\Core\Application\Event\RabbitMqEventBus;
use Interop\Container\ContainerInterface;

final class AsyncEventBusFactory
{
    /**
     * @param ContainerInterface $container
     * @return RabbitMqEventBus
     */
    public function __invoke(ContainerInterface $container)
    {
        return new RabbitMqEventBus($container);
    }
}