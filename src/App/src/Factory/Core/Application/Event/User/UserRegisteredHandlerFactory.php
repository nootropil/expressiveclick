<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Event\User;

use App\Core\Application\Event\User\UserRegisteredHandler;
use App\Core\Domain\Repository\User\UserRepository;
use App\Core\Domain\Repository\UserToken\UserTokenRepository;
use App\Core\Domain\Service\Notification\User\UserEmailNotificator;
use Interop\Container\ContainerInterface;

final class UserRegisteredHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return UserRegisteredHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        $userRepository = $container->get(UserRepository::class);
        $userTokenRepository = $container->get(UserTokenRepository::class);
        $userEmailNotificator = $container->get(UserEmailNotificator::class);
        return new UserRegisteredHandler($userRepository, $userTokenRepository, $userEmailNotificator);
    }
}