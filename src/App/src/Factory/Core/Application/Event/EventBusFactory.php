<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Event;

use App\Core\Application\Event\BaseEventBus;
use Interop\Container\ContainerInterface;

final class EventBusFactory
{
    /**
     * @param ContainerInterface $container
     * @return BaseEventBus
     */
    public function __invoke(ContainerInterface $container)
    {
        return new BaseEventBus($container);
    }
}