<?php
declare(strict_types=1);

namespace Localization\Factory\Action;

use Interop\Container\ContainerInterface;
use Localization\Action\DeleteLanguageAction;
use Localization\Core\Domain\Service\LanguageServiceInterface;
use Zend\Expressive\Router\RouterInterface;

final class DeleteLanguageActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return DeleteLanguageAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DeleteLanguageAction(
            $container->get(RouterInterface::class),
            $container->get(LanguageServiceInterface::class)
        );
    }
}