<?php
declare(strict_types=1);

namespace Localization\Factory\Action;

use Interop\Container\ContainerInterface;
use Localization\Action\LocalizationMiddleware;
use Localization\Action\UpdateLanguageAction;
use Localization\Core\Domain\Repository\LanguageRepository;
use Localization\Core\Domain\Service\LanguageServiceInterface;
use Localization\Form\UpdateLanguageForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class LocalizationMiddlewareFactory
{
    /**
     * @param ContainerInterface $container
     * @return LocalizationMiddleware
     */
    public function __invoke(ContainerInterface $container)
    {
        return new LocalizationMiddleware(
            $container->get(LanguageRepository::class)
        );
    }
}