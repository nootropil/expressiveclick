<?php
declare(strict_types=1);

namespace Localization\Factory\Action;

use Interop\Container\ContainerInterface;
use Localization\Action\LanguagesAction;
use Localization\Core\Domain\Repository\LanguageRepository;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class LanguagesActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return LanguagesAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new LanguagesAction(
            $container->get(RouterInterface::class),
            $container->get(LanguageRepository::class),
            $container->get(TemplateRendererInterface::class)
        );
    }
}