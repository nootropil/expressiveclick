<?php
declare(strict_types=1);

namespace Localization\Factory\Action;

use Interop\Container\ContainerInterface;
use Localization\Action\UpdateLanguageAction;
use Localization\Core\Domain\Repository\LanguageRepository;
use Localization\Core\Domain\Service\LanguageServiceInterface;
use Localization\Form\UpdateLanguageForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class UpdateLanguageActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateLanguageAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new UpdateLanguageAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(UpdateLanguageForm::class),
            $container->get(LanguageServiceInterface::class),
            $container->get(LanguageRepository::class)
        );
    }
}