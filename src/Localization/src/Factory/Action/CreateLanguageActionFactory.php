<?php
declare(strict_types=1);

namespace Localization\Factory\Action;

use Interop\Container\ContainerInterface;
use Localization\Action\CreateLanguageAction;
use Localization\Core\Domain\Service\LanguageServiceInterface;
use Localization\Form\CreateLanguageForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class CreateLanguageActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateLanguageAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new CreateLanguageAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(CreateLanguageForm::class),
            $container->get(LanguageServiceInterface::class)
        );
    }
}