<?php
declare(strict_types=1);

namespace Localization\Factory\Core\Domain\Service;

use Interop\Container\ContainerInterface;
use Localization\Core\Application\Service\LanguageService;
use Localization\Core\Domain\Repository\LanguageRepository;

final class LanguageServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return LanguageService
     */
    public function __invoke(ContainerInterface $container)
    {
        return new LanguageService(
            $container->get(LanguageRepository::class)
        );
    }
}