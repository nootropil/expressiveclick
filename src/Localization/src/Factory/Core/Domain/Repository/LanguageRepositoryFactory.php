<?php
declare(strict_types=1);

namespace Localization\Factory\Core\Domain\Repository;

use Interop\Container\ContainerInterface;
use PDO;
use Localization\Core\Infrastructure\Repository\PdoLanguageRepository;

final class LanguageRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return PdoLanguageRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $connectionConfig = $config['db_connection'];
        $pdo = new PDO($connectionConfig);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return new PdoLanguageRepository($pdo);
    }
}