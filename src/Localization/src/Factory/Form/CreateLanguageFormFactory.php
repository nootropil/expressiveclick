<?php
declare(strict_types=1);

namespace Localization\Factory\Form;

use Aura\Session\Session;
use Localization\Core\Domain\Repository\LanguageRepository;
use Interop\Container\ContainerInterface;
use Localization\Form\CreateLanguageForm;

final class CreateLanguageFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateLanguageForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new CreateLanguageForm(null, ['csrf' => $session->getCsrfToken()], $container->get(LanguageRepository::class));
    }
}