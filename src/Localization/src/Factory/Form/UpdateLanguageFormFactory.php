<?php
declare(strict_types=1);

namespace Localization\Factory\Form;

use Localization\Core\Domain\Repository\LanguageRepository;
use Interop\Container\ContainerInterface;
use Localization\Form\UpdateLanguageForm;
use Aura\Session\Session;

final class UpdateLanguageFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateLanguageForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new UpdateLanguageForm(null, ['csrf' => $session->getCsrfToken()], $container->get(LanguageRepository::class));
    }
}