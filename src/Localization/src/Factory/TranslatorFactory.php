<?php
declare(strict_types=1);

namespace Localization\Factory;

use Aura\Session\Session;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\Translator;

final class TranslatorFactory
{
    /**
     * @param ContainerInterface $container
     * @return Translator
     */
    public function __invoke(ContainerInterface $container)
    {

        $translator = new Translator();
        $translator->addTranslationFile(
            'phparray',
            'locale/en_US/default.php',
            $textDomain = 'default',
            'en_US');


        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return $translator;
    }
}