<?php
declare(strict_types=1);

namespace Localization;


use Localization\Action\LocalizationMiddleware;
use Localization\Factory\Action\LocalizationMiddlewareFactory;

final class ConfigProvider
{
    /**
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
            'routes'       => $this->getRoutes(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'factories' => [
                \Localization\Core\Domain\Repository\LanguageRepository::class => \Localization\Factory\Core\Domain\Repository\LanguageRepositoryFactory::class,
                \Localization\Core\Domain\Service\LanguageServiceInterface::class => \Localization\Factory\Core\Domain\Service\LanguageServiceFactory::class,
                
                \Localization\Action\CreateLanguageAction::class => \Localization\Factory\Action\CreateLanguageActionFactory::class,
                \Localization\Action\DeleteLanguageAction::class => \Localization\Factory\Action\DeleteLanguageActionFactory::class,
                \Localization\Action\LanguagesAction::class => \Localization\Factory\Action\LanguagesActionFactory::class,
                \Localization\Action\UpdateLanguageAction::class => \Localization\Factory\Action\UpdateLanguageActionFactory::class,

                \Localization\Form\CreateLanguageForm::class => \Localization\Factory\Form\CreateLanguageFormFactory::class,
                \Localization\Form\UpdateLanguageForm::class => \Localization\Factory\Form\UpdateLanguageFormFactory::class,

                \Zend\I18n\Translator\Translator::class => \Localization\Factory\TranslatorFactory::class,

                LocalizationMiddleware::class => LocalizationMiddlewareFactory::class
            ],
        ];
    }


    public function getRoutes()
    {
        return [
            [
                'name' => 'localization.language-create',
                'path' => '/localization/language/create',
                'middleware' => [
                    \Localization\Action\CreateLanguageAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'localization.language-delete',
                'path' => '/localization/language/delete/{id}',
                'middleware' => [
                    \Localization\Action\DeleteLanguageAction::class
                ],
                'allowed_methods' => ['POST'],
            ],
            [
                'name' => 'localization.language-update',
                'path' => '/localization/language/update/{id}',
                'middleware' => [
                    \Localization\Action\UpdateLanguageAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'localization.languages',
                'path' => '/localization/languages',
                'middleware' => [
                    \Localization\Action\LanguagesAction::class
                ],
                'allowed_methods' => ['GET'],
            ]           
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'localization' => ['src/Localization/templates'],
            ],
        ];
    }
}
