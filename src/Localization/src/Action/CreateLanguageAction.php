<?php
declare(strict_types=1);

namespace Localization\Action;

use Localization\Core\Domain\Service\LanguageServiceInterface;
use Localization\Form\CreateLanguageForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class CreateLanguageAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var CreateLanguageForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var LanguageServiceInterface
     */
    private $languageService;

    /**
     * LanguagesAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface|null $template
     * @param CreateLanguageForm $form
     * @param LanguageServiceInterface $languageService
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        CreateLanguageForm $form,
        LanguageServiceInterface $languageService
    )
    {
        $this->languageService = $languageService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->languageService->createLanguage($this->form->getData());
            return new RedirectResponse($this->router->generateUri('localization.languages'));
        }
        return new HtmlResponse($this->template->render('localization::language/create', [
            'form' => $this->form,
        ]));
    }
}
