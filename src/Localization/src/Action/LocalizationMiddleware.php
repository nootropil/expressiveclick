<?php
declare(strict_types=1);

namespace Localization\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Locale;
use Localization\Core\Domain\Repository\LanguageRepository;
use Psr\Http\Message\ServerRequestInterface;

class LocalizationMiddleware implements MiddlewareInterface
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return mixed
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        // Get locale from route, fallback to the user's browser preference
        $urlPrefix = $request->getAttribute('lang');
        $language = $this->languageRepository->fetchByUrl($urlPrefix);

        if ($language === null) {
            $defaultLang = $this->languageRepository->fetchByMain();
            if ($defaultLang !== null) {
                Locale::setDefault($defaultLang->getLocale());
            } else {
                Locale::setDefault(
                    Locale::acceptFromHttp($request->getServerParams()['HTTP_ACCEPT_LANGUAGE'] ?? 'en_US'));
            }
        } else {
            Locale::setDefault($language->getLocale());
        }
        Locale::setDefault('en_US');
        //  var_dump($locale);die;
        // Store the locale as a request attribute
        return $delegate->process($request);
    }
}