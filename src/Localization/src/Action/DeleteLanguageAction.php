<?php
declare(strict_types=1);

namespace Localization\Action;

use Localization\Core\Domain\Service\LanguageServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class DeleteLanguageAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var LanguageServiceInterface
     */
    private $languageService;

    /**
     * UpdateLanguageAction constructor.
     * @param RouterInterface $router
     * @param LanguageServiceInterface $languageService
     */
    public function __construct(
        RouterInterface $router,
        LanguageServiceInterface $languageService
    )
    {
        $this->languageService = $languageService;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $this->languageService->removeLanguage($request->getAttribute('id'));
        return new RedirectResponse($this->router->generateUri('localization.languages'));
    }
}
