<?php
declare(strict_types=1);

namespace Localization\Action;

use Localization\Core\Domain\Repository\LanguageRepository;
use Localization\Core\Domain\Service\LanguageServiceInterface;
use Localization\Form\UpdateLanguageForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class UpdateLanguageAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var UpdateLanguageForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var LanguageServiceInterface
     */
    private $languageService;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * UpdateLanguageAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface $template
     * @param UpdateLanguageForm $form
     * @param LanguageServiceInterface $languageService
     * @param LanguageRepository $languageRepository
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        UpdateLanguageForm $form,
        LanguageServiceInterface $languageService,
        LanguageRepository $languageRepository
    )
    {
        $this->languageRepository = $languageRepository;
        $this->languageService = $languageService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     * @throws \HttpException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $id = $request->getAttribute('id');
        $language = $this->languageRepository->fetch($id);
        if ($language === null) {
            throw new \HttpException('Localization is not found');
        }
        $this->form->setLanguage($language);
        $this->form->setData($language->toArray());
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->languageService->updateLanguage(array_merge($this->form->getData(), ['id' => $id]));
            return new RedirectResponse($this->router->generateUri('localization.languages'));
        }
        return new HtmlResponse($this->template->render('localization::language/update', [
            'form' => $this->form,
        ]));
    }
}
