<?php
declare(strict_types=1);

namespace Localization\Action;

use Localization\Core\Application\Dto\LanguagesDto;
use Localization\Core\Domain\Repository\LanguageRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class LanguagesAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * DeleteLanguageAction constructor.
     * @param RouterInterface $router
     * @param LanguageRepository $languageRepository
     * @param TemplateRendererInterface $template
     */
    public function __construct(
        RouterInterface $router,
        LanguageRepository $languageRepository,
        TemplateRendererInterface $template
    )
    {
        $this->template = $template;
        $this->languageRepository = $languageRepository;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $languages = (new LanguagesDto($this->languageRepository->fetchAll()))->languages;
        return new HtmlResponse($this->template->render('localization::language/languages', [
            'languages' => $languages,
        ]));
    }
}
