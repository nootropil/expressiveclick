<?php
declare(strict_types=1);

namespace Localization\Form;

use Aura\Session\CsrfToken;
use Localization\Core\Domain\Model\Boolean;
use Localization\Core\Domain\Repository\LanguageRepository;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

final class CreateLanguageForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;
    /**
     * CreateLanguageForm constructor.
     * @param null $name
     * @param array $options
     * @param LanguageRepository $languageRepository
     */
    public function __construct($name = null, $options = [], LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;

        // we want to ignore the name passed
        parent::__construct('create-language', $options);


        $this->add([
            'name' => 'name',
            'type' => 'Text',
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'name' => 'url',
            'type' => 'Text',
            'options' => [
                'label' => 'Url',
            ],
        ]);

        $this->add([
            'name' => 'local',
            'type' => 'Text',
            'options' => [
                'label' => 'Local',
            ],
        ]);

        $this->add([
            'name' => 'main',
            'type' => 'checkbox',
            'options' => [
                'required' => false,
                'label' => 'Main',
                'checked_value' => Boolean::YES,
                'unchecked_value' =>  Boolean::NO,
            ],
        ]);

        $this->add([
            'name' => 'active',
            'type' => 'checkbox',
            'options' => [
                'required' => false,
                'label' => 'Active',
                'checked_value' => Boolean::YES,
                'unchecked_value' =>  Boolean::NO,
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submit-button',
            ],
        ]);

        $this->add([
            'name' => '_csrf',
            'type' => 'hidden',
            'attributes' => [
                'value' => $this->getOption('csrf')->getValue(),
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if ($this->languageRepository->existsByName($value)) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'Name is not unique',
                        ]
                    ]
                ],
            ],
            [
                'name' => 'url',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 2,
                        ],
                    ],
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if ($this->languageRepository->existsByUrl($value)) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'Url is not unique',
                        ]
                    ],
                   [
                        'name' => 'Regex',
                        'options' => array(
                            'pattern' => '/[a-zA-Z]+/',
                            'messages' => array(
                                \Zend\Validator\Regex::NOT_MATCH => "Url characters in address"
                            )
                        ),
                    ]
                ],
            ],

            [
                'name' => 'local',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 5,
                            'max' => 5,
                        ],
                    ],
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if ($this->languageRepository->existsByLocal($value)) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'Local is not unique',
                        ]
                    ],

                    [
                        'name' => 'Regex',
                        'options' => array(
                            'pattern' => '/[a-zA-Z-]+/',
                            'messages' => array(
                                \Zend\Validator\Regex::NOT_MATCH => "Local characters in address"
                            )
                        ),
                    ]
                ],
            ],

            [
                'name' => 'main',
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 1,
                        ]
                    ],
                ]

            ],

            [
                'name' => 'active',
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 1,
                        ]
                    ],
                ]

            ],

            [
                'name' => '_csrf',
                'require' => true,
                'validators' => [
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context, CsrfToken $csrf) {
                                if ($csrf->isValid($value)) {
                                    return true;
                                }

                                return false;
                            },
                            'callbackOptions' => [
                                $this->getOption('csrf'),
                            ],
                            'message' => 'The form submitted did not originate from the expected site',
                        ],
                    ],
                ],
            ],
        ];
    }
}
