<?php
declare(strict_types=1);

namespace Localization\Core\Infrastructure\Repository;

use Localization\Core\Domain\Model\Language;

final class LanguageHydrator
{
    /**
     * @param array $columns
     * @return Language
     */
    public function hydrate(array $columns): Language
    {
        return new Language(
            $columns['id'],
            $columns['url'],
            $columns['locale'],
            $columns['name'],
            $columns['main'],
            $columns['active']
        );
    }
}