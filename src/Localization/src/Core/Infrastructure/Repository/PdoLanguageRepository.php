<?php
declare(strict_types = 1);

namespace Localization\Core\Infrastructure\Repository;

use App\Core\Infrastructure\Service\IdentityGenerator\Uuid4Generator;
use Localization\Core\Domain\Model\Boolean;
use Localization\Core\Domain\Model\Language;
use Localization\Core\Domain\Repository\LanguageRepository;
use PDO;

final class PdoLanguageRepository implements LanguageRepository
{
    const TABLE_NAME = 'localization_language';

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var LanguageHydrator
     */
    private $hydrator;

    /**
     * PdoLanguageRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->hydrator = new LanguageHydrator();
    }

    /**
     * @param $id
     * @return Language|null
     */
    public function fetch(string $id): ?Language
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @return Language[]
     */
    public function fetchAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @return Language[]
     */
    public function fetchAllActive(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE active = :active');
        $stmt->execute([':active' => Boolean::YES]);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @return Language|null
     */
    public function fetchMain(): ?Language
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE main = :main');
        $stmt->execute([':main' => Boolean::YES]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $name
     * @return Language|null
     */
    public function fetchByName(string $name): ?Language
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE name = :name');
        $stmt->execute([':name' => $name]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function existsByName(string $name): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE name = :name');
        $stmt->execute([':name' => $name]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param string $url
     * @return Language|null
     */
    public function fetchByUrl(string $url): ?Language
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE url = :url');
        $stmt->execute([':url' => $url]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $url
     * @return bool
     */
    public function existsByUrl(string $url): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE url = :url');
        $stmt->execute([':url' => $url]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @return Language|null
     */
    public function fetchByMain(): ?Language
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE main = :main');
        $stmt->execute([':main' => Boolean::YES]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $locale
     * @return Language|null
     */
    public function fetchByLocal(string $locale): ?Language
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE locale = :locale');
        $stmt->execute([':locale' => $locale]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $locale
     * @return bool
     */
    public function existsByLocal(string $locale): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE locale = :locale');
        $stmt->execute([':locale' => $locale]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param Language $language
     */
    public function save(Language $language): void
    {
        $stmt = $this->pdo->prepare('UPDATE ' . self::TABLE_NAME . ' SET 
            url = :url, locale = :locale, name = :name, main = :main, active = :active WHERE id = :id');
        $stmt->bindValue(':id', $language->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':url', $language->getUrl(), PDO::PARAM_STR);
        $stmt->bindValue(':locale', $language->getLocale(), PDO::PARAM_STR);
        $stmt->bindValue(':name', $language->getName(), PDO::PARAM_STR);
        $stmt->bindValue(':main', $language->getMain(), PDO::PARAM_STR);
        $stmt->bindValue(':active', $language->getActive(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param Language $language
     */
    public function add(Language $language): void
    {
        $stmt = $this->pdo->prepare('INSERT INTO ' . self::TABLE_NAME . ' (id,  url, locale, name, main, active) VALUES
         (:id,  :url, :locale, :name, :main, :active)');
        $stmt->bindValue(':id', $language->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':url', $language->getUrl(), PDO::PARAM_STR);
        $stmt->bindValue(':locale', $language->getLocale(), PDO::PARAM_STR);
        $stmt->bindValue(':name', $language->getName(), PDO::PARAM_STR);
        $stmt->bindValue(':main', $language->getMain(), PDO::PARAM_STR);
        $stmt->bindValue(':active', $language->getActive(), PDO::PARAM_STR);
        $stmt->execute();
    }

    public function defineAllLanguagesAsNonMain(): void
    {
        $stmt = $this->pdo->prepare('UPDATE ' . self::TABLE_NAME . ' SET main = :main');
        $stmt->bindValue(':main', Boolean::NO, PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param Language $language
     */
    public function remove(Language $language): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE id = :id');
        $stmt->bindValue(':id', $language->getId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string
    {
        $uuidGenerator = new Uuid4Generator();
        return $uuidGenerator->generate();
    }

    /**
     * @param $result
     * @return Language
     */
    private function toEntity(array $result): Language
    {
        return $this->hydrator->hydrate($result);
    }
}