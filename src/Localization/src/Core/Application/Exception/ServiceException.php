<?php
declare(strict_types = 1);

namespace Localization\Core\Application\Exception;

final class ServiceException extends \Exception
{
}