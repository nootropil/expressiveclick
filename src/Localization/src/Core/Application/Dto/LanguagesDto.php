<?php
declare(strict_types = 1);

namespace Localization\Core\Application\Dto;

use Localization\Core\Domain\Model\Language;

final class LanguagesDto
{
    /**
     * @var LanguageDto[]
     */
    public $languages;

    /**
     * LanguagesDto constructor.
     * @param array $languages
     */
    public function __construct(array $languages)
    {
        /* @var $language Language */
        foreach ($languages as $language) {
            $this->languages[] = new LanguageDto($language);
        }
    }
}