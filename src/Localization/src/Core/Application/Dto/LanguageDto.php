<?php
declare(strict_types = 1);

namespace Localization\Core\Application\Dto;

use Localization\Core\Domain\Model\Language;

final class LanguageDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $locale;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $main;

    /**
     * @var string
     */
    public $active;

    /**
     * LanguageDto constructor.
     * @param Language $language
     */
    public function __construct(Language $language)
    {
        $this->id = $language->getId();
        $this->url = $language->getUrl();
        $this->locale = $language->getLocale();
        $this->name = $language->getName();
        $this->main = $language->getMain();
        $this->active = $language->getActive();
    }


}