<?php
declare(strict_types = 1);

namespace Localization\Core\Application\Service;

use Localization\Core\Application\Exception\ServiceException;
use Localization\Core\Domain\Model\Boolean;
use Localization\Core\Domain\Model\Language;
use Localization\Core\Domain\Repository\LanguageRepository;
use Localization\Core\Domain\Service\LanguageServiceInterface;

final class LanguageService implements LanguageServiceInterface
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * LanguageService constructor.
     * @param LanguageRepository $languageRepository
     */
    public function __construct(
        LanguageRepository $languageRepository
    )
    {
        $this->languageRepository = $languageRepository;
    }


    /**
     * @param array $data
     * @throws ServiceException
     */
    public function createLanguage(array $data) : void
    {
        $url = $data['url'];
        $locale = $data['locale'];
        $name = $data['name'];
        $main = $data['main'];
        $active = $data['active'];
        if ($this->languageRepository->existsByName($name)) {
            throw new ServiceException("Name already exists");
        }
        if ($this->languageRepository->existsByUrl($url)) {
            throw new ServiceException("Url already exists");
        }
        if ($this->languageRepository->existsByLocal($locale)) {
            throw new ServiceException("Local already exists");
        }
        if (
            ($main !== Boolean::YES && $main !== Boolean::NO) ||
            ($active !== Boolean::YES && $active !== Boolean::NO)
        ) {
            throw new ServiceException("Incorrect bool value");
        }
        $id = $this->languageRepository->nextIdentity();
        $language = new Language($id, $url, $locale, $name, $main, $active);
        if ($language->isMain()) {
            $this->languageRepository->defineAllLanguagesAsNonMain();
        }
        $this->languageRepository->add($language);
    }

    /**
     * @param array $data
     * @throws ServiceException
     */
    public function updateLanguage(array $data) : void
    {
        $id =  $data['id'];
        $url = $data['url'];
        $locale = $data['locale'];
        $name = $data['name'];
        $main = $data['main'];
        $active = $data['active'];
        $language = $this->languageRepository->fetch($id);
        $isMain = $language->isMain();
        $languageWithSuchName = $this->languageRepository->fetchByName($name);
        if ($languageWithSuchName !== null && $languageWithSuchName->getId() !== $language->getId()) {
            throw new ServiceException("Name already exists");
        }
        $languageWithSuchUrl = $this->languageRepository->fetchByUrl($url);
        if ($languageWithSuchUrl !== null && $languageWithSuchUrl->getId() !== $language->getId()) {
            throw new ServiceException("Url already exists");
        }
        $languageWithSuchLocale = $this->languageRepository->fetchByLocal($locale);
        if ($languageWithSuchLocale !== null && $languageWithSuchLocale->getId() !== $language->getId()) {
            throw new ServiceException("Locale already exists");
        }
        if (
            ($main !== Boolean::YES && $main !== Boolean::NO) ||
            ($active !== Boolean::YES && $active !== Boolean::NO)
        ) {
            throw new ServiceException("Incorrect bool value");
        }
        $language->changeUrl($url);
        $language->changeLocale($locale);
        $language->changeName($name);
        $language->changeMain($main);
        $language->changeActive($active);
        if (!$isMain && $language->isMain()) {
            $this->languageRepository->defineAllLanguagesAsNonMain();
        }
        $this->languageRepository->save($language);
    }

    /**
     * @param string $id
     * @throws ServiceException
     */
    public function removeLanguage(string $id) : void
    {
        $language = $this->languageRepository->fetch($id);
        if ($language === null) {
            throw new ServiceException("Localization does not exist");
        }
        $this->languageRepository->remove($language);
    }
}