<?php
declare(strict_types = 1);

namespace Localization\Core\Domain\Repository;

use Localization\Core\Domain\Model\Language;

interface LanguageRepository
{
    /**
     * @param $id
     * @return Language|null
     */
    public function fetch(string $id): ?Language;

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool;

    /**
     * @return Language[]
     */
    public function fetchAll(): array;

    /**
     * @return Language[]
     */
    public function fetchAllActive(): array;

    /**
     * @return Language|null
     */
    public function fetchMain(): ?Language;

    /**
     * @param string $name
     * @return Language|null
     */
    public function fetchByName(string $name): ?Language;

    /**
     * @param string $name
     * @return bool
     */
    public function existsByName(string $name): bool;

    /**
     * @param string $url
     * @return Language|null
     */
    public function fetchByUrl(string $url): ?Language;

    /**
     * @return Language|null
     */
    public function fetchByMain(): ?Language;

    /**
     * @param string $url
     * @return bool
     */
    public function existsByUrl(string $url): bool;

    /**
     * @param string $locale
     * @return Language|null
     */
    public function fetchByLocal(string $locale): ?Language;

    /**
     * @param string $locale
     * @return bool
     */
    public function existsByLocal(string $locale): bool;

    public function defineAllLanguagesAsNonMain(): void;

    /**
     * @param Language $language
     */
    public function save(Language $language): void;

    /**
     * @param Language $language
     */
    public function add(Language $language): void;

    /**
     * @param Language $language
     */
    public function remove(Language $language): void;

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string;
}