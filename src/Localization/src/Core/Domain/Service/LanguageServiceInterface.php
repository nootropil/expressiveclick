<?php
declare(strict_types = 1);

namespace Localization\Core\Domain\Service;

interface LanguageServiceInterface
{
    /**
     * @param array $data
     */
    public function createLanguage(array $data) : void;

    /**
     * @param array $data
     */
    public function updateLanguage(array $data) : void;

    /**
     * @param string $id
     */
    public function removeLanguage(string $id) : void;

}