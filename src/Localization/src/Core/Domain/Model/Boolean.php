<?php
declare(strict_types=1);

namespace Localization\Core\Domain\Model;

final class Boolean
{
    const YES = 'Y';
    const NO = 'N';
}