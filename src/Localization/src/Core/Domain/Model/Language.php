<?php
declare(strict_types=1);

namespace Localization\Core\Domain\Model;

final class Language
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $main;

    /**
     * @var string
     */
    private $active;

    /**
     * Localization constructor.
     * @param string $id
     * @param string $url
     * @param string $locale
     * @param string $name
     * @param string $main
     * @param string $active
     */
    public function __construct(
        string $id,
        string $url,
        string $locale,
        string $name,
        string $main,
        string $active
    )
    {
        $this->id = $id;
        $this->url = $url;
        $this->locale = $locale;
        $this->name = $name;
        $this->main = $main;
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isMain(): bool
    {
        return $this->main === Boolean::YES;
    }

    /**
     * @param string $url
     */
    public function changeUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @param string $locale
     */
    public function changeLocale(string $locale)
    {
        $this->locale = $locale;
    }

    /**
     * @param string $name
     */
    public function changeName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $main
     */
    public function changeMain(string $main)
    {
        $this->main = $main;
    }

    /**
     * @param string $active
     */
    public function changeActive(string $active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getMain(): string
    {
        return $this->main;
    }

    public function isActive() : bool
    {
        return $this->active === Boolean::YES;
    }

    /**
     * @return string
     */
    public function getActive(): string
    {
        return $this->active;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'url' => $this->url,
            'locale' => $this->locale,
            'name' => $this->name,
            'main' => $this->main,
            'active' => $this->active
        ];

    }

}