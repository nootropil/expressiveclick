<?php
declare(strict_types=1);

namespace Authorization\Action\Group;

use Authorization\Core\Domain\Service\GroupServiceInterface;
use Authorization\Form\CreateGroupForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class CreateGroupAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var CreateGroupForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var GroupServiceInterface
     */
    private $groupService;

    /**
     * GroupsAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface|null $template
     * @param CreateGroupForm $form
     * @param GroupServiceInterface $groupService
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        CreateGroupForm $form,
        GroupServiceInterface $groupService
    )
    {
        $this->groupService = $groupService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->groupService->createGroup($this->form->getData());
            return new RedirectResponse($this->router->generateUri('authorization.groups'));
        }
        return new HtmlResponse($this->template->render('authorization::group/create', [
            'form' => $this->form,
        ]));
    }
}
