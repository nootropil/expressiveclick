<?php
declare(strict_types=1);

namespace Authorization\Action\Group;

use Authorization\Core\Domain\Repository\GroupRepository;
use Authorization\Core\Domain\Service\GroupServiceInterface;
use Authorization\Form\UpdateGroupForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class UpdateGroupAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var UpdateGroupForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var GroupServiceInterface
     */
    private $groupService;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * UpdateGroupAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface $template
     * @param UpdateGroupForm $form
     * @param GroupServiceInterface $groupService
     * @param GroupRepository $groupRepository
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        UpdateGroupForm $form,
        GroupServiceInterface $groupService,
        GroupRepository $groupRepository
    )
    {
        $this->groupRepository = $groupRepository;
        $this->groupService = $groupService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     * @throws \HttpException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $id = $request->getAttribute('id');
        $group = $this->groupRepository->fetch($id);
        if ($group === null) {
            throw new \HttpException('Localization is not found');
        }
        $this->form->setGroup($group);
        $this->form->setData($group->toArray());
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->groupService->updateGroup(array_merge($this->form->getData(), ['id' => $id]));
            return new RedirectResponse($this->router->generateUri('authorization.groups'));
        }
        return new HtmlResponse($this->template->render('authorization::group/update', [
            'form' => $this->form,
        ]));
    }
}
