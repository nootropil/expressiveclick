<?php
declare(strict_types=1);

namespace Authorization\Action\PermissionCategory;

use Authorization\Core\Domain\Service\PermissionCategoryServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class DeletePermissionCategoryAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var PermissionCategoryServiceInterface
     */
    private $permissionCategoryService;

    /**
     * UpdatePermissionCategoryAction constructor.
     * @param RouterInterface $router
     * @param PermissionCategoryServiceInterface $permissionCategoryService
     */
    public function __construct(
        RouterInterface $router,
        PermissionCategoryServiceInterface $permissionCategoryService
    )
    {
        $this->permissionCategoryService = $permissionCategoryService;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $this->permissionCategoryService->removePermissionCategory($request->getAttribute('id'));
        return new RedirectResponse($this->router->generateUri('authorization.permissions-categories'));
    }
}
