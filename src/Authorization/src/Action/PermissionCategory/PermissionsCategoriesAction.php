<?php
declare(strict_types=1);

namespace Authorization\Action\PermissionCategory;

use Authorization\Core\Application\Dto\PermissionCategoriesDto;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class PermissionsCategoriesAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var PermissionCategoryRepository
     */
    private $permissionCategoryRepository;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * DeletePermissionCategoryAction constructor.
     * @param RouterInterface $router
     * @param PermissionCategoryRepository $permissionCategoryRepository
     * @param TemplateRendererInterface $template
     */
    public function __construct(
        RouterInterface $router,
        PermissionCategoryRepository $permissionCategoryRepository,
        TemplateRendererInterface $template
    )
    {
        $this->template = $template;
        $this->permissionCategoryRepository = $permissionCategoryRepository;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $permissionCategories = (new PermissionCategoriesDto($this->permissionCategoryRepository->fetchAll()))->permissionsCategories;
        return new HtmlResponse($this->template->render('authorization::permission-category/permissions-categories', [
            'permissionCategories' => $permissionCategories,
        ]));
    }
}
