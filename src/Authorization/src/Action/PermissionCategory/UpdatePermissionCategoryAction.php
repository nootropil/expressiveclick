<?php
declare(strict_types=1);

namespace Authorization\Action\PermissionCategory;

use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Service\PermissionCategoryServiceInterface;
use Authorization\Form\UpdatePermissionCategoryForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class UpdatePermissionCategoryAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var UpdatePermissionCategoryForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var PermissionCategoryServiceInterface
     */
    private $permissionCategoryService;

    /**
     * @var PermissionCategoryRepository
     */
    private $permissionCategoryRepository;

    /**
     * UpdatePermissionCategoryAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface $template
     * @param UpdatePermissionCategoryForm $form
     * @param PermissionCategoryServiceInterface $permissionCategoryService
     * @param PermissionCategoryRepository $permissionCategoryRepository
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        UpdatePermissionCategoryForm $form,
        PermissionCategoryServiceInterface $permissionCategoryService,
        PermissionCategoryRepository $permissionCategoryRepository
    )
    {
        $this->permissionCategoryRepository = $permissionCategoryRepository;
        $this->permissionCategoryService = $permissionCategoryService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     * @throws \HttpException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $id = $request->getAttribute('id');
        $permissionCategory = $this->permissionCategoryRepository->fetch($id);
        if ($permissionCategory === null) {
            throw new \HttpException('PermissionCategory is not found');
        }
        $this->form->setPermissionCategory($permissionCategory);
        $this->form->setData($permissionCategory->toArray());
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->permissionCategoryService->updatePermissionCategory(array_merge($this->form->getData(), ['id' => $id]));
            return new RedirectResponse($this->router->generateUri('authorization.permissions-categories'));
        }
        return new HtmlResponse($this->template->render('authorization::permission-category/update', [
            'form' => $this->form,
        ]));
    }
}
