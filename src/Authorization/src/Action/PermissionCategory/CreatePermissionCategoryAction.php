<?php
declare(strict_types=1);

namespace Authorization\Action\PermissionCategory;

use Authorization\Core\Domain\Service\PermissionCategoryServiceInterface;
use Authorization\Form\CreatePermissionCategoryForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class CreatePermissionCategoryAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var CreatePermissionCategoryForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var PermissionCategoryServiceInterface
     */
    private $permissionCategoryService;

    /**
     * PermissionCategorysAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface|null $template
     * @param CreatePermissionCategoryForm $form
     * @param PermissionCategoryServiceInterface $permissionCategoryService
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        CreatePermissionCategoryForm $form,
        PermissionCategoryServiceInterface $permissionCategoryService
    )
    {
        $this->permissionCategoryService = $permissionCategoryService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->permissionCategoryService->createPermissionCategory($this->form->getData());
            return new RedirectResponse($this->router->generateUri('authorization.permissions-categories'));
        }
        return new HtmlResponse($this->template->render('authorization::permission-category/create', [
            'form' => $this->form,
        ]));
    }
}
