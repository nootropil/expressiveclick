<?php
declare(strict_types=1);

namespace Authorization\Action\Permission;

use Authorization\Core\Domain\Repository\PermissionRepository;
use Authorization\Core\Domain\Service\PermissionServiceInterface;
use Authorization\Form\UpdatePermissionForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class UpdatePermissionAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var UpdatePermissionForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var PermissionServiceInterface
     */
    private $permissionService;

    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    /**
     * UpdatePermissionAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface $template
     * @param UpdatePermissionForm $form
     * @param PermissionServiceInterface $permissionService
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        UpdatePermissionForm $form,
        PermissionServiceInterface $permissionService,
        PermissionRepository $permissionRepository
    )
    {
        $this->permissionRepository = $permissionRepository;
        $this->permissionService = $permissionService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     * @throws \HttpException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $id = $request->getAttribute('id');
        $permission = $this->permissionRepository->fetch($id);
        if ($permission === null) {
            throw new \HttpException('Permission is not found');
        }

        $this->form->setPermission($permission);
        $this->form->setData($permission->toArray());
        $this->form->setData($request->getParsedBody());

        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->permissionService->updatePermission(array_merge($this->form->getData(), ['id' => $id]));
            return new RedirectResponse($this->router->generateUri('authorization.permissions'));
        }
        return new HtmlResponse($this->template->render('authorization::permission/update', [
            'form' => $this->form,
        ]));
    }
}
