<?php
declare(strict_types=1);

namespace Authorization\Action\Permission;

use Authorization\Core\Domain\Service\PermissionServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class DeletePermissionAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var PermissionServiceInterface
     */
    private $permissionService;

    /**
     * UpdatePermissionAction constructor.
     * @param RouterInterface $router
     * @param PermissionServiceInterface $permissionService
     */
    public function __construct(
        RouterInterface $router,
        PermissionServiceInterface $permissionService
    )
    {
        $this->permissionService = $permissionService;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $this->permissionService->removePermission($request->getAttribute('id'));
        return new RedirectResponse($this->router->generateUri('authorization.permissions'));
    }
}
