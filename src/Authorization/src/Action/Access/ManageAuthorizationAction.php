<?php

namespace Authorization\Action\Access;

use Authentication\Action\AuthAction;
use Authorization\Core\Infrastructure\Provider\UserPermissionsProviderInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;

class ManageAuthorizationAction implements MiddlewareInterface
{
    /**
     * @var UserPermissionsProviderInterface
     */
    private $userPermissionsProvider;

    /**
     * ManageAuthorizationAction constructor.
     * @param UserPermissionsProviderInterface $userPermissionsProvider
     */
    public function __construct(UserPermissionsProviderInterface $userPermissionsProvider)
    {
        $this->userPermissionsProvider = $userPermissionsProvider;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return \Psr\Http\Message\ResponseInterface|EmptyResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        return $delegate->process($request);//TODO for debug

        $user = $request->getAttribute(AuthAction::class, false);
        if (false === $user) {
            return new EmptyResponse(401);
        }
        $userPermissions = $this->userPermissionsProvider->getPermissionsFromDb($user['userId']);
        if (empty($userPermissions) || !in_array('manage.permissions', $userPermissions)) {
            return new EmptyResponse(403);
        }
        return $delegate->process($request);
    }
}