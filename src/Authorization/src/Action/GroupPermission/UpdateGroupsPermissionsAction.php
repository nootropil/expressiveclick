<?php
declare(strict_types=1);

namespace Authorization\Action\GroupPermission;

use Authorization\Core\Application\Dto\GroupsDto;
use Authorization\Core\Application\Dto\PermissionCategoriesDto;
use Authorization\Core\Application\Dto\PermissionsDto;
use Authorization\Core\Domain\Repository\GroupPermissionRepository;
use Authorization\Core\Domain\Repository\GroupRepository;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Authorization\Core\Domain\Service\GroupPermissionServiceInterface;
use Authorization\Form\UpdateGroupsPermissionsForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class UpdateGroupsPermissionsAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var UpdateGroupsPermissionsForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var GroupPermissionServiceInterface
     */
    private $groupPermissionService;

    /**
     * @var GroupPermissionRepository
     */
    private $groupPermissionRepository;

    /**
     * @var PermissionCategoryRepository
     */
    private $permissionCategoryRepository;

    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * UpdateGroupsPermissionsAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface $template
     * @param UpdateGroupsPermissionsForm $form
     * @param GroupPermissionServiceInterface $groupPermissionService
     * @param GroupPermissionRepository $groupPermissionRepository
     * @param PermissionCategoryRepository $permissionCategoryRepository
     * @param PermissionRepository $permissionRepository
     * @param GroupRepository $groupRepository
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        UpdateGroupsPermissionsForm $form,
        GroupPermissionServiceInterface $groupPermissionService,
        GroupPermissionRepository $groupPermissionRepository,
        PermissionCategoryRepository $permissionCategoryRepository,
        PermissionRepository $permissionRepository,
        GroupRepository $groupRepository
    )
    {
        $this->permissionCategoryRepository = $permissionCategoryRepository;
        $this->permissionRepository = $permissionRepository;
        $this->groupPermissionRepository = $groupPermissionRepository;
        $this->groupPermissionService = $groupPermissionService;
        $this->groupRepository = $groupRepository;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     * @throws \HttpException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $categories = (new PermissionCategoriesDto($this->permissionCategoryRepository->fetchAll()))->permissionsCategories;

        $groups = (new GroupsDto($this->groupRepository->fetchAll()))->groups;
        $permissions = (new PermissionsDto($this->permissionRepository->fetchAll()))->permissions;
        $groupsPermissions = $this->groupPermissionRepository->fetchAll();
        $gPArray = [];
        foreach ($groupsPermissions as $groupPermission) {
            $gPArray[] = $groupPermission->getGroupId() . '|' . $groupPermission->getPermissionId();
        }

        $this->form->setCheckbox($groups, $permissions, $gPArray);
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->groupPermissionService->updateGroupsPermissions($this->prepareArray($request->getParsedBody()));
            return new RedirectResponse($this->router->generateUri('authorization.update-groups-permissions'));
        }
        return new HtmlResponse($this->template->render('authorization::group-permission/update-groups-permissions', [
            'form' => $this->form,
            'categories' => $categories,
            'groups' => $groups,
            'permissions' => $permissions
        ]));
    }

    private function prepareArray(array $array): array
    {
        $result = [];
        foreach ($array as $key => $value) {
            if ($value === 'yes') {
                list($groupId, $permissionId) = explode('|', $key);
                if (array_key_exists($groupId, $result)) {
                    $result[$groupId][] = $permissionId;
                } else {
                    $result[$groupId] = [$permissionId];
                }
            }
        }
        return $result;
    }
}
