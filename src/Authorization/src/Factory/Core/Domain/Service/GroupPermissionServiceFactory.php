<?php
declare(strict_types=1);

namespace Authorization\Factory\Core\Domain\Service;

use Interop\Container\ContainerInterface;
use Authorization\Core\Application\Service\GroupPermissionService;
use Authorization\Core\Domain\Repository\GroupPermissionRepository;
use Authorization\Core\Domain\Repository\GroupRepository;
use Authorization\Core\Domain\Repository\PermissionRepository;

final class GroupPermissionServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return GroupPermissionService
     */
    public function __invoke(ContainerInterface $container)
    {
        return new GroupPermissionService(
            $container->get(GroupPermissionRepository::class),
            $container->get(PermissionRepository::class),
            $container->get(GroupRepository::class)
        );
    }
}