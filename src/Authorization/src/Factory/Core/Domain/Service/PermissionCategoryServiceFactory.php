<?php
declare(strict_types=1);

namespace Authorization\Factory\Core\Domain\Service;

use Interop\Container\ContainerInterface;
use Authorization\Core\Application\Service\PermissionCategoryService;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;

final class PermissionCategoryServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return PermissionCategoryService
     */
    public function __invoke(ContainerInterface $container)
    {
        return new PermissionCategoryService(
            $container->get(PermissionCategoryRepository::class)
        );
    }
}