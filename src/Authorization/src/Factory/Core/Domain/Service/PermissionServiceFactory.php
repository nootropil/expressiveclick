<?php
declare(strict_types=1);

namespace Authorization\Factory\Core\Domain\Service;

use Interop\Container\ContainerInterface;
use Authorization\Core\Application\Service\PermissionService;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Repository\PermissionRepository;

final class PermissionServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return PermissionService
     */
    public function __invoke(ContainerInterface $container)
    {
        return new PermissionService(
            $container->get(PermissionRepository::class),
            $container->get(PermissionCategoryRepository::class)
        );
    }
}