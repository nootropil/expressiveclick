<?php
declare(strict_types=1);

namespace Authorization\Factory\Core\Domain\Repository;

use Interop\Container\ContainerInterface;
use PDO;
use Authorization\Core\Infrastructure\Repository\PdoPermissionRepository;

final class PermissionRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return PdoPermissionRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $connectionConfig = $config['db_connection'];
        $pdo = new PDO($connectionConfig);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return new PdoPermissionRepository($pdo);
    }
}