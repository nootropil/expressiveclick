<?php
declare(strict_types=1);

namespace Authorization\Factory\Form;

use Authorization\Core\Domain\Repository\GroupRepository;
use Interop\Container\ContainerInterface;
use Authorization\Form\UpdateGroupForm;
use Aura\Session\Session;

final class UpdateGroupFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateGroupForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new UpdateGroupForm(null, ['csrf' => $session->getCsrfToken()], $container->get(GroupRepository::class));
    }
}