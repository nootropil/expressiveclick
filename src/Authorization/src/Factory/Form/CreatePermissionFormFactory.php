<?php
declare(strict_types=1);

namespace Authorization\Factory\Form;

use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Interop\Container\ContainerInterface;
use Authorization\Form\CreatePermissionForm;
use Aura\Session\Session;

final class CreatePermissionFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreatePermissionForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new CreatePermissionForm(
            null,
            ['csrf' => $session->getCsrfToken()],
            $container->get(PermissionCategoryRepository::class),
            $container->get(PermissionRepository::class)
        );
    }
}