<?php
declare(strict_types=1);

namespace Authorization\Factory\Form;

use Aura\Session\Session;
use Authorization\Core\Domain\Repository\GroupRepository;
use Interop\Container\ContainerInterface;
use Authorization\Form\CreateGroupForm;

final class CreateGroupFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateGroupForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new CreateGroupForm(null, ['csrf' => $session->getCsrfToken()], $container->get(GroupRepository::class));
    }
}