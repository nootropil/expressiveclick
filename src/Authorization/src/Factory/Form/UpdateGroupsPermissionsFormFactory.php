<?php
declare(strict_types=1);

namespace Authorization\Factory\Form;

use Authorization\Core\Domain\Repository\GroupPermissionRepository;
use Interop\Container\ContainerInterface;
use Authorization\Form\UpdateGroupsPermissionsForm;
use Aura\Session\Session;

final class UpdateGroupsPermissionsFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateGroupsPermissionsForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new UpdateGroupsPermissionsForm(null, ['csrf' => $session->getCsrfToken()], $container->get(GroupPermissionRepository::class));
    }
}