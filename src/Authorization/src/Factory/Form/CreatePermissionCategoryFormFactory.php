<?php
declare(strict_types=1);

namespace Authorization\Factory\Form;

use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Interop\Container\ContainerInterface;
use Authorization\Form\CreatePermissionCategoryForm;
use Aura\Session\Session;

final class CreatePermissionCategoryFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreatePermissionCategoryForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new CreatePermissionCategoryForm(null, ['csrf' => $session->getCsrfToken()],  $container->get(PermissionCategoryRepository::class));
    }
}