<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Access;

use Authorization\Action\Access\ManageAuthorizationAction;
use Authorization\Core\Infrastructure\Provider\UserPermissionsProviderInterface;
use Interop\Container\ContainerInterface;

final class ManageAuthorizationActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return ManageAuthorizationAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new ManageAuthorizationAction(
            $container->get(UserPermissionsProviderInterface::class)
        );
    }
}