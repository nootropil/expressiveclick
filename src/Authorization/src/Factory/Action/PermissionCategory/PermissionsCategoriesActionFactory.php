<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\PermissionCategory;

use Authorization\Action\PermissionCategory\PermissionsCategoriesAction;
use Interop\Container\ContainerInterface;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class PermissionsCategoriesActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return PermissionsCategoriesAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new PermissionsCategoriesAction(
            $container->get(RouterInterface::class),
            $container->get(PermissionCategoryRepository::class),
            $container->get(TemplateRendererInterface::class)
        );
    }
}