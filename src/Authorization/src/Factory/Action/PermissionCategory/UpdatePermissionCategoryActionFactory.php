<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\PermissionCategory;

use Authorization\Action\PermissionCategory\UpdatePermissionCategoryAction;
use Interop\Container\ContainerInterface;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Service\PermissionCategoryServiceInterface;
use Authorization\Form\UpdatePermissionCategoryForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class UpdatePermissionCategoryActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdatePermissionCategoryAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new UpdatePermissionCategoryAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(UpdatePermissionCategoryForm::class),
            $container->get(PermissionCategoryServiceInterface::class),
            $container->get(PermissionCategoryRepository::class)
        );
    }
}