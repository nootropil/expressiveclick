<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\PermissionCategory;

use Authorization\Action\PermissionCategory\CreatePermissionCategoryAction;
use Interop\Container\ContainerInterface;
use Authorization\Core\Domain\Service\PermissionCategoryServiceInterface;
use Authorization\Form\CreatePermissionCategoryForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class CreatePermissionCategoryActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreatePermissionCategoryAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new CreatePermissionCategoryAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(CreatePermissionCategoryForm::class),
            $container->get(PermissionCategoryServiceInterface::class)
        );
    }
}