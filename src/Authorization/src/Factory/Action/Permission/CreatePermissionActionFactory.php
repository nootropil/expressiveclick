<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Permission;

use Interop\Container\ContainerInterface;
use Authorization\Action\Permission\CreatePermissionAction;
use Authorization\Core\Domain\Service\PermissionServiceInterface;
use Authorization\Form\CreatePermissionForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class CreatePermissionActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreatePermissionAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new CreatePermissionAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(CreatePermissionForm::class),
            $container->get(PermissionServiceInterface::class)
        );
    }
}