<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Permission;

use Interop\Container\ContainerInterface;
use Authorization\Action\Permission\DeletePermissionAction;
use Authorization\Core\Domain\Service\PermissionServiceInterface;
use Zend\Expressive\Router\RouterInterface;

final class DeletePermissionActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return DeletePermissionAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DeletePermissionAction(
            $container->get(RouterInterface::class),
            $container->get(PermissionServiceInterface::class)
        );
    }
}