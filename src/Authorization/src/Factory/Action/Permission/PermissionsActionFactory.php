<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Permission;

use Interop\Container\ContainerInterface;
use Authorization\Action\Permission\PermissionsAction;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class PermissionsActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return PermissionsAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new PermissionsAction(
            $container->get(RouterInterface::class),
            $container->get(PermissionRepository::class),
            $container->get(TemplateRendererInterface::class)
        );
    }
}