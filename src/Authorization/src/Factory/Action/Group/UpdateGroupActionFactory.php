<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Group;

use Interop\Container\ContainerInterface;
use Authorization\Action\Group\UpdateGroupAction;
use Authorization\Core\Domain\Repository\GroupRepository;
use Authorization\Core\Domain\Service\GroupServiceInterface;
use Authorization\Form\UpdateGroupForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class UpdateGroupActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateGroupAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new UpdateGroupAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(UpdateGroupForm::class),
            $container->get(GroupServiceInterface::class),
            $container->get(GroupRepository::class)
        );
    }
}