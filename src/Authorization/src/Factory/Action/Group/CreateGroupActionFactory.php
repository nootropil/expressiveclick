<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Group;

use Interop\Container\ContainerInterface;
use Authorization\Action\Group\CreateGroupAction;
use Authorization\Core\Domain\Service\GroupServiceInterface;
use Authorization\Form\CreateGroupForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class CreateGroupActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateGroupAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new CreateGroupAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(CreateGroupForm::class),
            $container->get(GroupServiceInterface::class)
        );
    }
}