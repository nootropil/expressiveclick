<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Group;

use Interop\Container\ContainerInterface;
use Authorization\Action\Group\GroupsAction;
use Authorization\Core\Domain\Repository\GroupRepository;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class GroupsActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return GroupsAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new GroupsAction(
            $container->get(RouterInterface::class),
            $container->get(GroupRepository::class),
            $container->get(TemplateRendererInterface::class)
        );
    }
}