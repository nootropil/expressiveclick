<?php
declare(strict_types=1);

namespace Authorization\Form;

use Aura\Session\CsrfToken;
use Authorization\Core\Domain\Repository\GroupRepository;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

final class CreateGroupForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var GroupRepository
     */
    private $groupRepository;
    /**
     * CreateGroupForm constructor.
     * @param null $name
     * @param array $options
     * @param GroupRepository $groupRepository
     */
    public function __construct($name = null, $options = [], GroupRepository $groupRepository)
    {
        $this->groupRepository = $groupRepository;

        // we want to ignore the name passed
        parent::__construct('create-group', $options);


        $this->add([
            'name' => 'name',
            'type' => 'Text',
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'name' => 'description',
            'type' => 'Text',
            'options' => [
                'label' => 'Description',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submit-button',
            ],
        ]);

        $this->add([
            'name' => '_csrf',
            'type' => 'hidden',
            'attributes' => [
                'value' => $this->getOption('csrf')->getValue(),
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if ($this->groupRepository->existsByName($value)) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'Name is not unique',
                        ]
                    ]
                ],
            ],
            [
                'name' => 'description',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if ($this->groupRepository->existsByDescription($value)) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'Description is not unique',
                        ]
                    ]
                ],
            ],

            [
                'name' => '_csrf',
                'require' => true,
                'validators' => [
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context, CsrfToken $csrf) {
                                if ($csrf->isValid($value)) {
                                    return true;
                                }

                                return false;
                            },
                            'callbackOptions' => [
                                $this->getOption('csrf'),
                            ],
                            'message' => 'The form submitted did not originate from the expected site',
                        ],
                    ],
                ],
            ],
        ];
    }
}
