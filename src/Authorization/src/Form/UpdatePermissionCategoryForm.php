<?php
declare(strict_types=1);

namespace Authorization\Form;

use Aura\Session\CsrfToken;
use Authorization\Core\Domain\Model\PermissionCategory;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

final class UpdatePermissionCategoryForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var PermissionCategoryRepository
     */
    private $repository;

    /**
     * @var PermissionCategory
     */
    private $permissionCategory;

    /**
     * UpdatePermissionCategoryForm constructor.
     * @param null $name
     * @param array $options
     * @param PermissionCategoryRepository $repository
     */
    public function __construct($name = null, $options = [], PermissionCategoryRepository $repository)
    {
        $this->repository = $repository;
        // we want to ignore the name passed
        parent::__construct('update-permission-category', $options);


        $this->add([
            'name' => 'name',
            'type' => 'Text',
            'options' => [
                'label' => 'name',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Update',
                'id' => 'submit-button',
            ],
        ]);

        $this->add([
            'name' => '_csrf',
            'type' => 'hidden',
            'attributes' => [
                'value' => $this->getOption('csrf')->getValue(),
            ],
        ]);
    }

    /**
     * @param PermissionCategory $permissionCategory
     */
    public function setPermissionCategory(PermissionCategory $permissionCategory)
    {
        $this->permissionCategory = $permissionCategory;
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                $permissionCategory = $this->repository->fetchByName($value);
                                if ($permissionCategory !== null && $permissionCategory->getId() !== $this->permissionCategory->getId()) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'Name is not unique',
                        ]
                    ],
                ],
            ],
            [
                'name' => '_csrf',
                'require' => true,
                'validators' => [
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context, CsrfToken $csrf) {
                                if ($csrf->isValid($value)) {
                                    return true;
                                }

                                return false;
                            },
                            'callbackOptions' => [
                                $this->getOption('csrf'),
                            ],
                            'message' => 'The form submitted did not originate from the expected site',
                        ],
                    ],
                ],
            ],
        ];
    }
}
