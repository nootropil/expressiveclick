<?php
declare(strict_types=1);

namespace Authorization\Form;

use Aura\Session\CsrfToken;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

final class CreatePermissionForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var PermissionCategoryRepository
     */
    private $permissionCategoryRepository;

    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    /**
     * CreatePermissionForm constructor.
     * @param null $name
     * @param array $options
     * @param PermissionCategoryRepository $permissionCategoryRepository
     * @param PermissionRepository $permissionRepository
     */
    public function __construct($name = null, $options = [], PermissionCategoryRepository $permissionCategoryRepository, PermissionRepository $permissionRepository)
    {
        $this->permissionCategoryRepository = $permissionCategoryRepository;
        $this->permissionRepository = $permissionRepository;

        // we want to ignore the name passed
        parent::__construct('create-permission', $options);


        $this->add([
            'name' => 'name',
            'type' => 'Text',
            'options' => [
                'label' => 'name',
            ],
        ]);

        $this->add([
            'name' => 'description',
            'type' => 'Text',
            'options' => [
                'label' => 'description',
            ],
        ]);

        $this->add([
            'name' => 'permissionCategoryId',
            'type' => 'select',
            'options' => [
                'label' => 'permissionCategoryId',
                'empty_option' => 'Please choose category',
                'value_options' => array_merge(['' => ' - '], $permissionCategoryRepository->fetchAllAsIdNameArray()),
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submit-button',
            ],
        ]);

        $this->add([
            'name' => '_csrf',
            'type' => 'hidden',
            'attributes' => [
                'value' => $this->getOption('csrf')->getValue(),
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ]
                    ],
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if ($this->permissionRepository->existsByName($value)) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'Name is not unique',
                        ]
                    ]
                ]
            ],
            [
                'name' => 'description',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if ($this->permissionRepository->existsByDescription($value)) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'Description is not unique',
                        ]
                    ]
                ]
            ],

            [
                'name' => 'permissionCategoryId',
                'required' => true,
                'error_message' => 'Choose category',
                'validators' => [
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if (array_key_exists($value, $this->permissionCategoryRepository->fetchAllAsIdNameArray())) {
                                    return true;
                                }
                                return false;
                            },
                            'callbackOptions' => [
                                $this->getOption('permissionCategoryId'),
                            ],
                            'message' => 'This category does not exists',
                        ]
                    ]
                ]
            ],

            [
                'name' => '_csrf',
                'require' => true,
                'validators' => [
                    [
                        'name' => 'callback',
                        'options' => [
                            'callback' => function ($value, $context, CsrfToken $csrf) {
                                if ($csrf->isValid($value)) {
                                    return true;
                                }

                                return false;
                            },
                            'callbackOptions' => [
                                $this->getOption('csrf'),
                            ],
                            'message' => 'The form submitted did not originate from the expected site',
                        ]
                    ]
                ]
            ]
        ];
    }
}
