<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use Authorization\Core\Domain\Model\UserGroup;

final class UserGroupHydrator
{
    /**
     * @param array $columns
     * @return UserGroup
     */
    public function hydrate(array $columns): UserGroup
    {
        return new UserGroup(
            $columns['user_id'],
            $columns['group_id']
        );
    }
}