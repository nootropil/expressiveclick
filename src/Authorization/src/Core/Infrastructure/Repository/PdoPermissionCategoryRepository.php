<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use App\Core\Infrastructure\Service\IdentityGenerator\Uuid4Generator;
use Authorization\Core\Domain\Model\PermissionCategory;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use PDO;

final class PdoPermissionCategoryRepository implements PermissionCategoryRepository
{
    const TABLE_NAME = 'auth_permission_category';

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var PermissionCategoryHydrator
     */
    private $hydrator;

    /**
     * PdoPermissionCategoryRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->hydrator = new PermissionCategoryHydrator();
    }

    /**
     * @param $id
     * @return PermissionCategory|null
     */
    public function fetch(string $id): ?PermissionCategory
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @return PermissionCategory[]
     */
    public function fetchAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @return array
     */
    public function fetchAllAsIdNameArray(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $array = [];
        foreach ($rows as $row) {
            $array[$row['id']] = $row['name'] ;
        }
        return $array;
    }

    /**
     * @param string $name
     * @return PermissionCategory|null
     */
    public function fetchByName(string $name): ?PermissionCategory
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE name = :name');
        $stmt->execute([':name' => $name]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function existsByName(string $name): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE name = :name');
        $stmt->execute([':name' => $name]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param PermissionCategory $permissionCategory
     */
    public function save(PermissionCategory $permissionCategory): void
    {
        $stmt = $this->pdo->prepare('UPDATE ' . self::TABLE_NAME . ' SET name = :name WHERE id = :id');
        $stmt->bindValue(':id', $permissionCategory->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':name', $permissionCategory->getName(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param PermissionCategory $permissionCategory
     */
    public function add(PermissionCategory $permissionCategory): void
    {
        $stmt = $this->pdo->prepare('INSERT INTO ' . self::TABLE_NAME . ' (id, name) VALUES (:id, :name)');
        $stmt->bindValue(':id', $permissionCategory->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':name', $permissionCategory->getName(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param PermissionCategory $permissionCategory
     */
    public function remove(PermissionCategory $permissionCategory): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE id = :id');
        $stmt->bindValue(':id', $permissionCategory->getId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string
    {
        $uuidGenerator = new Uuid4Generator();
        return $uuidGenerator->generate();
    }

    /**
     * @param $result
     * @return PermissionCategory
     */
    private function toEntity(array $result): PermissionCategory
    {
        return $this->hydrator->hydrate($result);
    }
}