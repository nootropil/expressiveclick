<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use Authorization\Core\Domain\Model\GroupPermission;
use Authorization\Core\Domain\Repository\GroupPermissionRepository;
use PDO;

final class PdoGroupPermissionRepository implements GroupPermissionRepository
{
    const TABLE_NAME = 'auth_group_permission';

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var GroupPermissionHydrator
     */
    private $hydrator;

    /**
     * PdoGroupPermissionRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->hydrator = new GroupPermissionHydrator();
    }

    /**
     * @return GroupPermission[]
     */
    public function fetchAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @param string $groupId
     * @return GroupPermission[]
     */
    public function fetchAllByGroup(string $groupId): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME. ' WHERE group_id = :group_id');
        $stmt->bindValue(':group_id', $groupId, PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @param GroupPermission $groupPermission
     */
    public function add(GroupPermission $groupPermission): void
    {
        $stmt = $this->pdo->prepare('INSERT INTO ' . self::TABLE_NAME . ' (group_id, permission_id) VALUES (:group_id, :permission_id)');
        $stmt->bindValue(':group_id', $groupPermission->getGroupId(), PDO::PARAM_STR);
        $stmt->bindValue(':permission_id', $groupPermission->getPermissionId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param GroupPermission $groupPermission
     */
    public function remove(GroupPermission $groupPermission): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE permission_id = :permission_id AND  group_id = :group_id');
        $stmt->bindValue(':permission_id', $groupPermission->getPermissionId(), PDO::PARAM_STR);
        $stmt->bindValue(':group_id', $groupPermission->getGroupId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param $result
     * @return GroupPermission
     */
    private function toEntity(array $result): GroupPermission
    {
        return $this->hydrator->hydrate($result);
    }
}