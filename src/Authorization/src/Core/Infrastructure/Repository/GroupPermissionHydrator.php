<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use Authorization\Core\Domain\Model\GroupPermission;

final class GroupPermissionHydrator
{
     /**
     * @param array $columns
     * @return GroupPermission
     */
    public function hydrate(array $columns): GroupPermission
    {
        return new GroupPermission(
            $columns['group_id'],
            $columns['permission_id']
        );
    }
}