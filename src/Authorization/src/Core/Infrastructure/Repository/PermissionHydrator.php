<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use Authorization\Core\Domain\Model\Permission;

final class PermissionHydrator
{
    /**
     * @param array $columns
     * @return Permission
     */
    public function hydrate(array $columns): Permission
    {
        return new Permission(
            $columns['id'],
            $columns['name'],
            $columns['description'],
            $columns['permission_category_id']
        );
    }
}