<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use Authorization\Core\Domain\Model\UserGroup;
use Authorization\Core\Domain\Repository\UserGroupRepository;
use PDO;

final class PdoUserGroupRepository implements UserGroupRepository
{
    const TABLE_NAME = 'auth_user_group';

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var UserGroupHydrator
     */
    private $hydrator;

    /**
     * PdoUserGroupRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->hydrator = new UserGroupHydrator();
    }

    /**
     * @param string $userId
     * @param string $groupId
     * @return UserGroup|null
     */
    public function fetch(string $userId, string $groupId): ?UserGroup
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE user_id = :user_id AND group_id = :group_id');
        $stmt->execute([':user_id' => $userId]);
        $stmt->execute([':group_id' => $groupId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @return UserGroup[]
     */
    public function fetchAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @param string $userId
     * @return UserGroup[]
     */
    public function fetchAllPermissionByUserAsArray(string $userId): array
    {
        $stmt = $this->pdo->prepare('
          SELECT p.name as permission FROM auth_permission p
          JOIN RIGHT WITH auth_group_permission gp ON p.id = gp.permission_id
          JOIN RIGHT WITH auth_user_group ug ON gp.group_id = ug.group_id WHERE ug.user_id = :user_id');
        $stmt->execute([':user_id' => $userId]);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($rows as $row) {
            $result[] = $row['permission'];
        }
        return $result;
    }


    /**
     * @param string $userId
     * @param string $groupId
     * @return bool
     */
    public function exists(string $userId, string $groupId): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE user_id = :user_id AND group_id = :group_id');
        $stmt->execute([':user_id' => $userId]);
        $stmt->execute([':group_id' => $groupId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param string $userId
     * @return UserGroup[]
     */
    public function fetchAllByUser(string $userId): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE user_id = :user_id');
        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @param UserGroup $userGroup
     */
    public function add(UserGroup $userGroup): void
    {
        $stmt = $this->pdo->prepare('INSERT INTO ' . self::TABLE_NAME . ' (group_id, user_id) VALUES (:group_id, :user_id)');
        $stmt->bindValue(':group_id', $userGroup->getGroupId(), PDO::PARAM_STR);
        $stmt->bindValue(':user_id', $userGroup->getUserId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param UserGroup $userGroup
     */
    public function remove(UserGroup $userGroup): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE user_id = :user_id AND group_id = :group_id');
        $stmt->bindValue(':user_id', $userGroup->getUserId(), PDO::PARAM_STR);
        $stmt->bindValue(':group_id', $userGroup->getGroupId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param $result
     * @return UserGroup
     */
    private function toEntity(array $result): UserGroup
    {
        return $this->hydrator->hydrate($result);
    }
}