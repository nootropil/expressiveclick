<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use Authorization\Core\Domain\Model\Group;

final class GroupHydrator
{
    /**
     * @param array $columns
     * @return Group
     */
    public function hydrate(array $columns): Group
    {
        return new Group(
            $columns['id'],
            $columns['name'],
            $columns['description']
        );
    }
}