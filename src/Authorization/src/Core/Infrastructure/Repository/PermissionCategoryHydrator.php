<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use Authorization\Core\Domain\Model\PermissionCategory;

final class PermissionCategoryHydrator
{
    /**
     * @param array $columns
     * @return PermissionCategory
     */
    public function hydrate(array $columns): PermissionCategory
    {
        return new PermissionCategory(
            $columns['id'],
            $columns['name']
        );
    }
}