<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use App\Core\Infrastructure\Service\IdentityGenerator\Uuid4Generator;
use Authorization\Core\Domain\Model\Group;
use Authorization\Core\Domain\Repository\GroupRepository;
use PDO;

final class PdoGroupRepository implements GroupRepository
{
    const TABLE_NAME = 'auth_group';

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var GroupHydrator
     */
    private $hydrator;

    /**
     * PdoLanguageRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->hydrator = new GroupHydrator();
    }

    /**
     * @param $id
     * @return Group|null
     */
    public function fetch(string $id): ?Group
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @return Group[]
     */
    public function fetchAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @param string $name
     * @return Group|null
     */
    public function fetchByName(string $name): ?Group
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE name = :name');
        $stmt->execute([':name' => $name]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function existsByName(string $name): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE name = :name');
        $stmt->execute([':name' => $name]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param string $description
     * @return Group|null
     */
    public function fetchByDescription(string $description): ?Group
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE description = :description');
        $stmt->execute([':description' => $description]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $description
     * @return bool
     */
    public function existsByDescription(string $description): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE description = :description');
        $stmt->execute([':description' => $description]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }


    /**
     * @param Group $group
     */
    public function save(Group $group): void
    {
        $stmt = $this->pdo->prepare('UPDATE ' . self::TABLE_NAME . ' SET name = :name, description = :description WHERE id = :id');
        $stmt->bindValue(':id', $group->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':name', $group->getName(), PDO::PARAM_STR);
        $stmt->bindValue(':description', $group->getDescription(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param Group $group
     */
    public function add(Group $group): void
    {
        $stmt = $this->pdo->prepare('INSERT INTO ' . self::TABLE_NAME . ' (id, name, description) VALUES (:id, :name, :description)');
        $stmt->bindValue(':id', $group->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':name', $group->getName(), PDO::PARAM_STR);
        $stmt->bindValue(':description', $group->getDescription(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param Group $group
     */
    public function remove(Group $group): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE id = :id');
        $stmt->bindValue(':id', $group->getId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string
    {
        $uuidGenerator = new Uuid4Generator();
        return $uuidGenerator->generate();
    }

    /**
     * @param $result
     * @return Group
     */
    private function toEntity(array $result): Group
    {
        return $this->hydrator->hydrate($result);
    }
}