<?php
declare(strict_types=1);

namespace Authorization\Core\Infrastructure\Provider;

use Authorization\Core\Domain\Repository\UserGroupRepository;
use Predis\Client;

final class UserPermissionsProvider implements UserPermissionsProviderInterface
{
    const ENABLE_CACHE = 'permission_cache_enabled';

    /**
     * @var UserGroupRepository
     */
    private $userGroupRepository;

    /**
     * @var Client
     */
    private $redis;

    /**
     * UserPermissionsProvider constructor.
     * @param UserGroupRepository $userGroupRepository
     * @param Client $redis
     */
    public function __construct(UserGroupRepository $userGroupRepository, Client $redis)
    {
        $this->redis = $redis;
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * @param string $userId
     * @return array
     */
    public function getPermissionsFromDb(string $userId) : array
    {
        return $this->userGroupRepository->fetchAllPermissionByUserAsArray($userId);
    }

    /**
     * @param string $userId
     * @return array
     */
    public function getPermissionsFromCache(string $userId) : array
    {
        if (!$this->redis->exists('permissions:' . $userId)) {
            $this->cachePermissions($userId);
        }
        return json_decode($this->redis->get('permissions:' . $userId), true);
    }

    /**
     * @param string $userId
     */
     private function cachePermissions(string $userId) : void
     {
         $data = $this->userGroupRepository->fetchAllPermissionByUserAsArray($userId);
         $this->redis->set('permissions:' . $userId, json_encode($data));
         $this->redis->expire('permissions:' . $userId, 3600);
     }
}