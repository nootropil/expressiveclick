<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Provider;

interface UserPermissionsProviderInterface
{
    /**
     * @param string $userId
     * @return array
     */
    public function getPermissionsFromDb(string $userId) : array;

    /**
     * @param string $userId
     * @return array
     */
    public function getPermissionsFromCache(string $userId) : array;
}