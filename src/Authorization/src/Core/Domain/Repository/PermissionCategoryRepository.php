<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Repository;

use Authorization\Core\Domain\Model\PermissionCategory;

interface PermissionCategoryRepository
{
    /**
     * @param $id
     * @return PermissionCategory|null
     */
    public function fetch(string $id): ?PermissionCategory;

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool;

    /**
     * @return PermissionCategory[]
     */
    public function fetchAll(): array;

    /**
     * @return array
     */
    public function fetchAllAsIdNameArray(): array;

    /**
     * @param string $name
     * @return PermissionCategory|null
     */
    public function fetchByName(string $name): ?PermissionCategory;

    /**
     * @param string $name
     * @return bool
     */
    public function existsByName(string $name): bool;

    /**
     * @param PermissionCategory $permissionCategory
     */
    public function save(PermissionCategory $permissionCategory): void;

    /**
     * @param PermissionCategory $permissionCategory
     */
    public function add(PermissionCategory $permissionCategory): void;

    /**
     * @param PermissionCategory $permissionCategory
     */
    public function remove(PermissionCategory $permissionCategory): void;

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string;
}