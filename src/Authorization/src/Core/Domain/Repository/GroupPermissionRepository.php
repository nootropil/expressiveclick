<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Repository;

use Authorization\Core\Domain\Model\GroupPermission;

interface GroupPermissionRepository
{
    /**
     * @return GroupPermission[]
     */
    public function fetchAll(): array;

    /**
     * @param string $groupId
     * @return GroupPermission[]
     */
    public function fetchAllByGroup(string $groupId): array;

    /**
     * @param GroupPermission $groupPermission
     */
    public function add(GroupPermission $groupPermission): void;

    /**
     * @param GroupPermission $groupPermission
     */
    public function remove(GroupPermission $groupPermission): void;

}