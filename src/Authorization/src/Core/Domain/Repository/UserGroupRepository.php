<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Repository;

use Authorization\Core\Domain\Model\UserGroup;

interface UserGroupRepository
{
    /**
     * @param string $userId
     * @param string $groupId
     * @return UserGroup|null
     */
    public function fetch(string $userId, string $groupId): ?UserGroup;

    /**
     * @return UserGroup[]
     */
    public function fetchAll(): array;

    /**
     * @param string $userId
     * @return UserGroup[]
     */
    public function fetchAllByUser(string $userId): array;

    /**
     * @param string $userId
     * @return UserGroup[]
     */
    public function fetchAllPermissionByUserAsArray(string $userId): array;

    /**
     * @param string $userId
     * @param string $groupId
     * @return bool
     */
    public function exists(string $userId, string $groupId): bool;

    /**
     * @param UserGroup $userGroup
     */
    public function add(UserGroup $userGroup): void;

    /**
     * @param UserGroup $userGroup
     */
    public function remove(UserGroup $userGroup): void;
}