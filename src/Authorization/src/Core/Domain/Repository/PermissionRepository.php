<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Repository;

use Authorization\Core\Domain\Model\Permission;

interface PermissionRepository
{
    /**
     * @param $id
     * @return Permission|null
     */
    public function fetch(string $id): ?Permission;

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool;

    /**
     * @return Permission[]
     */
    public function fetchAll(): array;

    /**
     * @param string $name
     * @return Permission|null
     */
    public function fetchByName(string $name): ?Permission;

    /**
     * @param string $name
     * @return bool
     */
    public function existsByName(string $name): bool;

    /**
     * @param string $description
     * @return Permission|null
     */
    public function fetchByDescription(string $description): ?Permission;

    /**
     * @param string $description
     * @return bool
     */
    public function existsByDescription(string $description): bool;

    /**
     * @param Permission $permission
     */
    public function save(Permission $permission): void;

    /**
     * @param Permission $permission
     */
    public function add(Permission $permission): void;

    /**
     * @param Permission $permission
     */
    public function remove(Permission $permission): void;

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string;
}