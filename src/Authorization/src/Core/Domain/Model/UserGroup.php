<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Model;


final class UserGroup
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $groupId;

    /**
     * UserGroup constructor.
     * @param string $userId
     * @param string $groupId
     */
    public function __construct(
        string $userId,
        string $groupId
    )
    {
        $this->userId = $userId;
        $this->groupId = $groupId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }
}