<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Model;


final class GroupPermission
{
    /**
     * @var string
     */
    private $groupId;

    /**
     * @var string
     */
    private $permissionId;

    /**
     * GroupPermission constructor.
     * @param string $groupId
     * @param string $permissionId
     */
    public function __construct(
        string $groupId,
        string $permissionId
    )
    {
        $this->groupId = $groupId;
        $this->permissionId = $permissionId;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function getPermissionId(): string
    {
        return $this->permissionId;
    }


}