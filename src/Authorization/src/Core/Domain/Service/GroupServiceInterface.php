<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Service;

interface GroupServiceInterface
{
    /**
     * @param array $data
     */
    public function createGroup(array $data) : void;

    /**
     * @param array $data
     */
    public function updateGroup(array $data) : void;

    /**
     * @param string $name
     */
    public function removeGroup(string $name) : void;

}