<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Service;

interface GroupPermissionServiceInterface
{
    /**
     * @param array $data
     */
    public function updateGroupsPermissions(array $data) : void;

}