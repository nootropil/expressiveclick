<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Service;

interface PermissionServiceInterface
{
    /**
     * @param array $data
     */
    public function createPermission(array $data) : void;

    /**
     * @param array $data
     */
    public function updatePermission(array $data) : void;

    /**
     * @param string $name
     */
    public function removePermission(string $name) : void;
}