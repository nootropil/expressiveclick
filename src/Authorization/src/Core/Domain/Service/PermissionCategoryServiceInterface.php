<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Service;

interface PermissionCategoryServiceInterface
{
    /**
     * @param array $data
     */
    public function createPermissionCategory(array $data) : void;

    /**
     * @param array $data
     */
    public function updatePermissionCategory(array $data) : void;

    /**
     * @param string $id
     */
    public function removePermissionCategory(string $id) : void;

}