<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Dto;

use Authorization\Core\Domain\Model\Permission;

final class PermissionsDto
{
    /**
     * @var PermissionDto[]
     */
    public $permissions;

    /**
     * PermissionsDto constructor.
     * @param array $permissions
     */
    public function __construct(array $permissions)
    {
        /* @var $group Permission */
        foreach ($permissions as $permission) {
            $this->permissions[] = new PermissionDto($permission);
        }
    }
}