<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Dto;

final class PermissionCategoriesDto
{
    /**
     * @var PermissionCategoryDto[]
     */
    public $permissionsCategories;

    /**
     * PermissionCategoriesDto constructor.
     * @param array $permissionsCategories
     */
    public function __construct(array $permissionsCategories)
    {
        /* @var $group PermissionCategoryDto */
        foreach ($permissionsCategories as $permissionCategory) {
            $this->permissionsCategories[] = new PermissionCategoryDto($permissionCategory);
        }
    }
}