<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Dto;

use Authorization\Core\Domain\Model\Permission;

final class PermissionDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $permissionCategoryId;

    /**
     * PermissionDto constructor.
     * @param Permission $permission
     */
    public function __construct(Permission $permission)
    {
        $this->id = $permission->getId();
        $this->name = $permission->getName();
        $this->description = $permission->getDescription();
        $this->permissionCategoryId = $permission->getPermissionCategoryId();
    }
}