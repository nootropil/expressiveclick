<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Dto;

use Authorization\Core\Domain\Model\Group;

final class GroupDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * LanguageDto constructor.
     * @param Group $group
     */
    public function __construct(Group $group)
    {
        $this->id = $group->getId();
        $this->name = $group->getName();
        $this->description = $group->getDescription();
    }
}