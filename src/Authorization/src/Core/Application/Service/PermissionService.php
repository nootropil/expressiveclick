<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Service;

use Authorization\Core\Application\Exception\ServiceException;
use Authorization\Core\Domain\Model\Permission;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Authorization\Core\Domain\Service\PermissionServiceInterface;

final class PermissionService implements PermissionServiceInterface
{
    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    /**
     * @var PermissionCategoryRepository
     */
    private $permissionCategoryRepository;

    /**
     * PermissionService constructor.
     * @param PermissionRepository $permissionRepository
     * @param PermissionCategoryRepository $permissionCategoryRepository
     */
    public function __construct(
        PermissionRepository $permissionRepository,
        PermissionCategoryRepository $permissionCategoryRepository
    )
    {
        $this->permissionCategoryRepository = $permissionCategoryRepository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * @param array $data
     * @throws ServiceException
     */
    public function createPermission(array $data) : void
    {
        $description =  $data['description'];
        $name = $data['name'];
        $permissionCategoryId = $data['permissionCategoryId'];

        if ($this->permissionRepository->existsByDescription($description)) {
            throw new ServiceException("Description already exists");
        }
        if ($this->permissionRepository->existsByName($name)) {
            throw new ServiceException("Name already exists");
        }
        if (!$this->permissionCategoryRepository->exists($permissionCategoryId)) {
            throw new ServiceException("The category does not exist");
        }

        $id = $this->permissionRepository->nextIdentity();
        $permission = new Permission(
            $id,
            $name,
            $description,
            $permissionCategoryId
        );

        $this->permissionRepository->add($permission);
    }

    /**
     * @param array $data
     * @throws ServiceException
     */
    public function updatePermission(array $data) : void
    {
        $id =  $data['id'];
        $description =  $data['description'];
        $name = $data['name'];
        $permissionCategoryId = $data['permissionCategoryId'];
        $permission = $this->permissionRepository->fetch($id);

        $permissionWithSuchDescription = $this->permissionRepository->fetchByDescription($description);
        if ($permissionWithSuchDescription !== null && $permissionWithSuchDescription->getId() !== $permission->getId()) {
            throw new ServiceException("Description already exists");
        }
        $permissionWithSuchName = $this->permissionRepository->fetchByName($name);
        if ($permissionWithSuchName !== null && $permissionWithSuchName->getId() !== $permission->getId()) {
            throw new ServiceException("Name already exists");
        }
        if (!$this->permissionCategoryRepository->exists($permissionCategoryId)) {
            throw new ServiceException("The category does not exist");
        }

        $permission->changeDescription($description);
        $permission->changeName($name);
        $permission->changePermissionCategoryId($permissionCategoryId);
        $this->permissionRepository->save($permission);
    }

    /**
     * @param string $id
     * @throws ServiceException
     */
    public function removePermission(string $id) : void
    {
        $permission = $this->permissionRepository->fetch($id);

        if ($permission === null) {
            throw new ServiceException("Permission does not exist");
        }

        $this->permissionRepository->remove($permission);
    }
}