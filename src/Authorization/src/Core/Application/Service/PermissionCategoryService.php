<?php
declare(strict_types=1);

namespace Authorization\Core\Application\Service;

use Authorization\Core\Application\Exception\ServiceException;
use Authorization\Core\Domain\Model\PermissionCategory;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Service\PermissionCategoryServiceInterface;

final class PermissionCategoryService implements PermissionCategoryServiceInterface
{
    /**
     * @var PermissionCategoryRepository
     */
    private $permissionCategoryRepository;

    /**
     * PermissionCategoryService constructor.
     * @param PermissionCategoryRepository $permissionCategoryRepository
     */
    public function __construct(
        PermissionCategoryRepository $permissionCategoryRepository
    )
    {
        $this->permissionCategoryRepository = $permissionCategoryRepository;
    }

    /**
     * @param array $data
     * @throws ServiceException
     */
    public function createPermissionCategory(array $data): void
    {
        $name = $data['name'];

        if ($this->permissionCategoryRepository->existsByName($name)) {
            throw new ServiceException("Name already exists");
        }

        $id = $this->permissionCategoryRepository->nextIdentity();
        $permissionCategory = new PermissionCategory($id, $name);
        $this->permissionCategoryRepository->add($permissionCategory);
    }

    /**
     * @param array $data
     * @throws ServiceException
     */
    public function updatePermissionCategory(array $data): void
    {
        $id = $data['id'];
        $name = $data['name'];
        $permissionCategory = $this->permissionCategoryRepository->fetch($id);

        $categoryWithSuchName = $this->permissionCategoryRepository->fetchByName($name);
        if ($categoryWithSuchName !== null && $categoryWithSuchName->getId() !== $permissionCategory->getId()) {
            throw new ServiceException("Name already exists");
        }

        $permissionCategory->changeName($name);
        $this->permissionCategoryRepository->save($permissionCategory);
    }

    /**
     * @param string $id
     * @throws ServiceException
     */
    public function removePermissionCategory(string $id): void
    {
        $permissionCategory = $this->permissionCategoryRepository->fetch($id);

        if ($permissionCategory === null) {
            throw new ServiceException("PermissionCategory does not exist");
        }

        $this->permissionCategoryRepository->remove($permissionCategory);
    }
}