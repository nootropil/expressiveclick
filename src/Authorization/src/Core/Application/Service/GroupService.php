<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Service;

use Authorization\Core\Application\Exception\ServiceException;
use Authorization\Core\Domain\Model\Group;
use Authorization\Core\Domain\Repository\GroupRepository;
use Authorization\Core\Domain\Service\GroupServiceInterface;

final class GroupService implements GroupServiceInterface
{
    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * LanguageService constructor.
     * @param GroupRepository $groupRepository
     */
    public function __construct(
        GroupRepository $groupRepository
    )
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * @param array $data
     * @throws ServiceException
     */
    public function createGroup(array $data) : void
    {
        $name = $data['name'];
        $description = $data['description'];
        if ($this->groupRepository->existsByName($name)) {
            throw new ServiceException("Name already exists");
        }
        if ($this->groupRepository->existsByDescription($description)) {
            throw new ServiceException("Description already exists");
        }
        $id = $this->groupRepository->nextIdentity();
        $group = new Group($id, $name, $description);
        $this->groupRepository->add($group);
    }

    /**
     * @param array $data
     * @throws ServiceException
     */
    public function updateGroup(array $data) : void
    {
        $id =  $data['id'];
        $name = $data['name'];
        $description = $data['description'];
        $group = $this->groupRepository->fetch($id);

        $groupWithSuchName = $this->groupRepository->fetchByName($name);
        if ($groupWithSuchName !== null && $groupWithSuchName->getId() !== $group->getId()) {
            throw new ServiceException("Name already exists");
        }
        $groupWithSuchDescription = $this->groupRepository->fetchByDescription($description);
        if ($groupWithSuchDescription !== null && $groupWithSuchDescription->getId() !== $group->getId()) {
            throw new ServiceException("Name already exists");
        }

        $group->changeName($name);
        $group->changeDescription($description);
        $this->groupRepository->save($group);
    }

    /**
     * @param string $id
     * @throws ServiceException
     */
    public function removeGroup(string $id) : void
    {
        $group = $this->groupRepository->fetch($id);

        if ($group === null) {
            throw new ServiceException("Group does not exist");
        }

        $this->groupRepository->remove($group);
    }
}