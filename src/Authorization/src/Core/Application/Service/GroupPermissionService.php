<?php
declare(strict_types=1);

namespace Authorization\Core\Application\Service;

use Authorization\Core\Application\Exception\ServiceException;
use Authorization\Core\Domain\Model\GroupPermission;
use Authorization\Core\Domain\Repository\GroupPermissionRepository;
use Authorization\Core\Domain\Repository\GroupRepository;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Authorization\Core\Domain\Service\GroupPermissionServiceInterface;

final class GroupPermissionService implements GroupPermissionServiceInterface
{
    /**
     * @var GroupPermissionRepository
     */
    private $groupPermissionRepository;
    /**
     * @var PermissionRepository
     */
    private $permissionRepository;
    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * GroupPermissionService constructor.
     * @param GroupPermissionRepository $groupPermissionRepository
     * @param PermissionRepository $permissionRepository
     * @param GroupRepository $groupRepository
     */
    public function __construct(
        GroupPermissionRepository $groupPermissionRepository,
        PermissionRepository $permissionRepository,
        GroupRepository $groupRepository
    )
    {
        $this->groupPermissionRepository = $groupPermissionRepository;
        $this->permissionRepository = $permissionRepository;
        $this->groupRepository = $groupRepository;
    }

    /**
     * $data = array('groupId 1' => array('permissionId 1', ...), ...)
     * @param array $data
     * @throws ServiceException
     */
    public function updateGroupsPermissions(array $data): void
    {
        $groupsPermissions = $this->groupPermissionRepository->fetchAll();
        $preparedArray = [];
        foreach ($groupsPermissions as $groupPermission) {
            $preparedArray[$groupPermission->getGroupId() . $groupPermission->getPermissionId()] = $groupPermission;
        }
        foreach ($data as $groupId => $permissionsIds) {

            if (!$this->groupRepository->exists($groupId)) {
                throw new ServiceException("Group does not exist");
            }

            foreach ($permissionsIds as $permissionId) {

                if (!$this->permissionRepository->exists($permissionId)) {
                    throw new ServiceException("Permission does not exist");
                }

                $key = $groupId . $permissionId;
                if (!array_key_exists($key, $preparedArray)) {
                    $this->groupPermissionRepository->add(new GroupPermission($groupId, $permissionId));
                } else {
                    unset($preparedArray[$key]);
                }
            }
        }
        foreach ($preparedArray as $permissionToRemove) {
            $this->groupPermissionRepository->remove($permissionToRemove);
        }
    }

}