<?php
declare(strict_types=1);

namespace Authentication\Action;

use Authentication\Form\LoginForm;
use Authentication\Service\MyAuthAdapter;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class LoginAction implements MiddlewareInterface
{
    /**
     * @var AuthenticationService
     */
    private $auth;

    /**
     * @var MyAuthAdapter
     */
    private $authAdapter;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var LoginForm
     */
    private $form;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * LoginAction constructor.
     * @param TemplateRendererInterface $template
     * @param AuthenticationService $auth
     * @param MyAuthAdapter $authAdapter
     * @param LoginForm $form
     * @param RouterInterface $router
     */
    public function __construct(
        TemplateRendererInterface $template,
        AuthenticationService $auth,
        MyAuthAdapter $authAdapter,
        LoginForm $form,
        RouterInterface $router
    )
    {
        $this->template = $template;
        $this->auth = $auth;
        $this->authAdapter = $authAdapter;
        $this->form = $form;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return HtmlResponse|RedirectResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $params = $request->getParsedBody();
        $this->form->setData($params);
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->authAdapter->setUsername($params['username']);
            $this->authAdapter->setPassword($params['password']);
            $result = $this->auth->authenticate();
            if (!$result->isValid()) {
                return new HtmlResponse($this->template->render('auth::login', [
                    'username' => $params['username'],
                    'error' => 'The credentials provided are not valid',
                ]));
            }
            return new RedirectResponse($this->router->generateUri('index'));
        }
        return new HtmlResponse($this->template->render('authentication::login', ['form' => $this->form]));
    }

    /**
     * @param ServerRequestInterface $request
     * @return HtmlResponse|RedirectResponse
     */
    public function authenticate(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();

        $this->authAdapter->setUsername($params['username']);
        $this->authAdapter->setPassword($params['password']);
        $result = $this->auth->authenticate();
        if (!$result->isValid()) {
            return new HtmlResponse($this->template->render('authentication::login', [
                'username' => $params['username'],
                'error' => 'The credentials provided are not valid',
            ]));
        }
    }
}