<?php
declare(strict_types=1);

namespace Authentication\Service;

use App\Core\Domain\Repository\User\UserReadRepository;
use Whoops\Exception\ErrorException;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

final class MyAuthAdapter implements AdapterInterface
{
    private $password;
    private $username;

    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * MyAuthAdapter constructor.
     * @param UserReadRepository $userReadRepository
     */
    public function __construct(UserReadRepository $userReadRepository)
    {
        $this->userReadRepository = $userReadRepository;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password) : void
    {
        $this->password = $password;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username) : void
    {
        $this->username = $username;
    }

    /**
     * @return Result
     * @throws ErrorException
     */
    public function authenticate()
    {
        $user = $this->userReadRepository->fetchByUsername($this->username);
        if ($user !== null) {
            if (password_verify($this->password, $user->getPasswordHash())) {
                return new Result(Result::SUCCESS, new Identity($user));
            }
        }
        return new Result(Result::FAILURE_CREDENTIAL_INVALID, $this->username);
    }
}