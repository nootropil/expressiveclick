<?php
declare(strict_types=1);

namespace Authentication\Factory;

use App\Core\Domain\Repository\User\UserReadRepository;
use Authentication\Service\MyAuthAdapter;
use Interop\Container\ContainerInterface;

final class MyAuthAdapterFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $userReadRepository = $container->get(UserReadRepository::class);
        return new MyAuthAdapter($userReadRepository);
    }
}