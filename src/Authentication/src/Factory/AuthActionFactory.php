<?php
declare(strict_types=1);

namespace Authentication\Factory;

use Authentication\Action\AuthAction;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;

final class AuthActionFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new AuthAction($container->get(AuthenticationService::class));
    }
}