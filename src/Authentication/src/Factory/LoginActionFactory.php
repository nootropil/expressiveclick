<?php
declare(strict_types=1);

namespace Authentication\Factory;

use Authentication\Action\LoginAction;
use Authentication\Form\LoginForm;
use Authentication\Service\MyAuthAdapter;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class LoginActionFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new LoginAction(
            $container->get(TemplateRendererInterface::class),
            $container->get(AuthenticationService::class),
            $container->get(MyAuthAdapter::class),
            $container->get(LoginForm::class),
            $container->get(RouterInterface::class)
        );
    }
}