<?php
declare(strict_types=1);

namespace Authentication;

/**
 * The configuration provider for the Auth module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
final class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
            ],
            'factories' => [
                \Authentication\Service\MyAuthAdapter::class => \Authentication\Factory\MyAuthAdapterFactory::class,
                \Zend\Authentication\AuthenticationService::class => \Authentication\Factory\AuthenticationServiceFactory::class,
                \Authentication\Action\LoginAction::class => \Authentication\Factory\LoginActionFactory::class,
                \Authentication\Action\AuthAction::class => \Authentication\Factory\AuthActionFactory::class,
                \Authentication\Form\LoginForm::class => \Authentication\Factory\LoginFormFactory::class
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'authentication' => ['src/Authentication/templates'],
            ],
        ];
    }
}
