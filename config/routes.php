<?php
/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Action\HomePageAction::class, 'home');
 * $app->post('/album', App\Action\AlbumCreateAction::class, 'album.create');
 * $app->put('/album/:id', App\Action\AlbumUpdateAction::class, 'album.put');
 * $app->patch('/album/:id', App\Action\AlbumUpdateAction::class, 'album.patch');
 * $app->delete('/album/:id', App\Action\AlbumDeleteAction::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Action\ContactAction::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */


$app->route('/login', Authentication\Action\LoginAction::class, ['GET', 'POST'], 'login');

$app->route('/user/registration', [
    Authentication\Action\AuthAction::class,
    App\Presentation\Action\User\RegistrationAction::class
], ['GET', 'POST'], 'registration');

$app->route('/user/email-conformation/{token}', App\Presentation\Action\User\EmailConfirmationAction::class, ['GET'], 'email-conformation');
$app->route('/user/password-recovering/{token}', App\Presentation\Action\User\PasswordRecoveringAction::class, ['GET', 'POST'], 'password-recovering');


$app->route('/{lang}', [
    \Localization\Action\LocalizationMiddleware::class,
    App\Presentation\Action\Common\IndexAction::class
], ['GET'], 'index');



$app->route('/user/email-conformation-request', App\Presentation\Action\User\EmailConfirmationRequestAction::class, ['GET', 'POST'], 'email-conformation-request');
$app->route('/user/password-recovering-request', App\Presentation\Action\User\PasswordRecoveringRequestAction::class, ['GET', 'POST'], 'password-recovering-request');



$app->injectRoutesFromConfig((new \Authorization\ConfigProvider())());
$app->injectRoutesFromConfig((new \Localization\ConfigProvider())());

